\chapter{Apprendimento di concetti}

In questo capitolo ci si focalizza sull'apprendimento di concetti, che sono ipotesi fatte a partire da esempi riguardanti una popolazione di elementi della realtà omogenei e descritti tramite attributi (anche chiamati \emph{features}).
L'attributo di classe è, tra questi, uno di particolare importanza ed è usato per assegnare una categoria (una classe) all'esempio.\\
Il nostro obbiettivo è costruire una generalizzazione comune (meglio detta \emph{concetto}) che possa descrivere tutti gli esempi di una certa categoria (classe positiva) ma che sia nel contempo abbastanza restrittiva da escludere gli elementi appartenenti alla classe negativa.\\
Le descrizioni degli esempi su cui si lavora sono rappresentate dalla congiunzione di predicati logici (anche chiamati \emph{letterali}) della forma $attribute=value$ o $attribute<value$, il cui significato riteniamo essere intuitivo (lo è vero?!).\\
L'approccio alla generalizzazione è di tipo \emph{bottom-up}: partendo dalla descrizione di ogni esempio $i$ si escludono mano a mano tutti i letterali in conflitto con un altro esempio $j$.\\
Si costruiscono così diverse generalizzazioni, a seconda dell'ordine in cui gli esempi vengono considerati e si terrà in considerazione solo un set di generalizzazioni, ovvero quello che rappresenta la "Least General Generalization".\\
Si sfrutta in genere un grafo per avere visivamente chiaro i passaggi.

\section{Un primo esempio}\label{sec:ex}

Il primo esempio che verrà proposto avrà come scopo la classificazione degli animali, ed in particolare il riconoscimento della classe dei Delfini. \\
In questo primo esempio utilizzeremo letterali formati solamente da uguaglianza.
Supponiamo all'inizio di avere i seguenti attributi:
\begin{equation}
\begin{aligned}
& Length = \mbox{"valore numerico"} \land Branchie = "yes/no" \land \\
& Beak="yes/no" \land "Teeth = "few/many"
\end{aligned} \nonumber
\end{equation}

Non tutti gli esempi devono necessariamente istanziare tutti gli attributi ed è quindi possibile avere per alcuni attributi un valore indefinito.

Supponiamo di avere un set di esempi così composto:
\begin{enumerate}
	\item $
	Length = 3 (A1) \land Branchie = no (B1) \land Beak=yes (C1) \land Teeth = many (D1) 
	$
	\item $
	Length = 4 \land Branchie = no \land Beak=yes \land Teeth = many 
	$
	\item $
	Length = 3 \land Branchie = no \land Beak=yes \land Teeth = few 
	$
\end{enumerate}
Supponendo di sottoporre gli esempi nello stesso ordine in cui sono stati presentati, al primo passo avremo la prima generalizzazione sull'attributo "Lenght", che verrà rimosso, rimanendo con una descrizione di classe generica così formata:
$$
Gills = no \land Beak=yes \land Teeth = many 
$$
Il terzo esempio ci porta poi alla rimozione dell'attributo "Teeth" ed otterremo così la classe più generale composta da:
$$
Branchie = no \land Beak=yes
$$
Ogni esempio la cui descrizione soddisfa questa disgiunzione sarà classificato "Delfino" dal nostro sistema.

\section{Spazio delle ipotesi}
Lo spazio delle ipotesi è rappresentato dall'insieme delle specifiche di ogni possibile esempio del set preso in considerazione.\\
Per quanto riguarda l'esempio visto nella sezione \ref{sec:ex} possiamo ritrovare lo spazio delle ipotesi in Figura \ref{fig:hysp}.
\begin{figure}[!ht]
	\centering
	\includegraphics[width=1\linewidth]{Images/HypothesisSpace}
	\caption{Grafo raffigurante lo spazio delle ipotesi trattato nella sezione \ref{sec:ex}}
	\label{fig:hysp}
\end{figure}
Come si nota dal livello foglia, abbiamo in tutto $ 24 $ possibili istanze date  dalla congiunzione dei $ 4 $ letterali: con $ 3 $ possibili istanze dell'attributo "Length", e 2 per ciascun altro attributo, $ 24 = 3*2*2*2 $.
Se consideriamo anche l'assenza di valore per gli attributi, data dalla eliminazione dello stesso a seguito di una generalizzazione, avremo $ 4*3*3*3=108 $ possibili \emph{concetti} (rappresentati dai nodi intermedi del grafo). \\
Quindi, lo spazio delle ipotesi è esponenziale nel numero di attributi, ma lo spazio delle \emph{estensioni} possibili (numero di possibili insiemi di istanza che possiamo avere) è decisamente maggiore ($ 2^{24} > 16 milioni $).
Quindi abbiamo deciso di utilizzare un linguaggio per descrivere gli esempi, con un certo numero di attributi, così da ottenere un numero finito di concetti da rappresentare, ma questo ci ha portati ad avere un numero di possibili insiemi di esempi (che è possibile vengano sottoposti al sistema per essere appresi) decisamente più grande: questo può far pensare quanto è difficile apprendere concetti a partire dai dati.

Tuttavia il fatto che l'algoritmo di apprendimento debba generalizzare può giocare a nostro favore, in quanto in questo modo potrebbe autonomamente includere nella sua generalizzazione concetti che coprono anche esempi mai visti. \\
Continuiamo ora l'esempio e, partendo dal grafo, cerchiamo di restringere lo spazio delle ipotesi considerando solo quei concetti che coprono gli esempi del dataset di tre esempi dato. Ci restano $ 32 $ concetti, di cui solo $ 3 $ ricoprono la classe positiva.\\ %non sono per ora tutti appartenenti alla classe positiva e basta?
Ciò che viene suggerito dalla Figura \ref{fig:lgg} è esattamente quanto fatto nell'esempio in sezione \ref{sec:ex}, e poi proseguito fino all'ottenimento della generalizzazione $ TRUE $ (l'universo) che è si una generalizzazione, ma che intuitivamente include anche esempi della classe negativa. \\
\begin{figure}[!ht]
	\centering
	\includegraphics[width=1\linewidth]{./Images/LGG}
	\caption{Grafo raffigurante lo spazio delle ipotesi ristretto agli esempi della classe positiva}
	\label{fig:lgg}
\end{figure}
Intuitivamente la \acs{lgg} di due esempi è il concetto nello spazio delle ipotesi dove il cammino a partire da due o più esempi e verso la radice si interseca. Una proprietà utile assicura l'unicità di questo punto.\\
Più precisamente, nella struttura formata dallo spazio delle ipotesi avremo che due elementi hanno un \ac{lub} che è il minimo set di ipotesi in comune e un \ac{glb} che è invece il massimo set di ipotesi in comune. Se consideriamo, anziché lo spazio delle ipotesi, solamente le istanze riconducibili all'interno di questi de confini, l'\acs{lgg} è esattamente il \acs{glb} della struttura reticolare da essi formato (\ref{fig:lggex}).

\begin{figure}[!ht]
	\centering
	\includegraphics[width=1\linewidth]{./Images/LGGEx}
	\caption{Rappresentazione di \acs{lgg}, \acs{lub} e \acs{glb} in un reticolo formato dagli esempi.}
	\label{fig:lggex}
\end{figure}

Possiamo inoltre dire che tutte le possibile generalizzazioni sono almeno generali quanto l'\acs{lgg}, in tal senso è la generalizzazione più conservativa che il sistema può apprendere.

\subsection{Le relazioni d'ordine parziale}
Prima di continuare con il discorso intrapreso ed arrivare a definire l'algoritmo di costruzione di una \acs{lgg} daremo un paio di nozioni di background. Qualora vi sentiate particolarmente abili e non peccate di modestia avete il nostro permesso di saltare questa parte.
La prima nozione riguarda una relazione d'ordine parziale.
Una relazione binaria R definita sugli insieme A e B è un sottoinsieme del prodotto cartesiano $AxB$.
Un ordine parziale R è una relazione binaria definita sugli insiemi A e B che rispecchia le seguenti proprietà:
\begin{description}
	\item [Riflessività] ovvero $ \forall x \in A, (x, x) \in R $;
	\item [Antisimmetria] ovvero $ \forall x, y \in A,\ \mbox{se } (x, y) \in R\ \mbox{e } (y, x) \in R\ \mbox{allora } x = y $;
	\item [Transitività] ovvero $ \forall x, y, z \in A,\ \mbox{se } (x, y) \in R\ \mbox{e } (y, z) \in R\ \mbox{allora } (x, z) \in R $.
\end{description}

Per esempio, considerando
$ X = \{a,b,c,d\} $ 
e $ A = Insieme\_delle\_parti\_di\_X = \{\{a\}, \{b\}, \{c\}, \{d\}, \{a, b\}, \{a, c\}, \{a, d\}, \{b, c\}, \{b, d\}, \{c, d\}, \{a, b, c\}, \{a, b, d\}, \{a, c, d\}, \{b, c, d\}, \{a, b, c, d\}\}$
si può dare una rappresentazione a grafo per $ A $ secondo la relazione di inclusione. Se ne da una rappresentazione grafica in Figura \ref{fig:subset} in cui è inoltre possibile individuare \acs{lgg}, \acs{lub} e \acs{glb}.

\begin{figure}[!ht]
	\centering
	\includegraphics[width=1\textwidth]{Images/SubsetRelation}
	\caption{Relazione d'ordine parziale raffigurante l'inclusione di insiemi facenti parti dell'insieme delle parti di $ X=\{a,b,c,d\} $.}
	\label{fig:subset}
\end{figure}

\subsection{Proprietà dei concetti}

Se una espressione booleana A è vera per un esempio x (ovvero i letterali che descrivono x rispettano l'espressione) allora diremo che \emph{A copre x}. \\
Un insieme di esempi coperti da A è anche chiamato "\emph{estensione di A}" e si indica con $ \chi_{A} $ dove $ \chi $ indica lo spazio delle istanze considerato.

Si vanno ora ad elencare un insieme di proprietà utili:
\begin{itemize}
	\item $\chi _{A \wedge B} = \chi_A \bigcap\chi_B$;
	\item $\chi _{A \vee B} = \chi _A \bigcup \chi _B$;
	\item $\chi _{\not A } = \chi \ \chi _A$;
	\item se $\chi _A \supseteq \chi _B$ allora diremo che A è almeno generale quanto B;
	\item l'ordine di generalità è un ordine parziale nei letterali.
\end{itemize}

\section{Algoritmo \acs{lgg}}
Si può quindi definire l'algoritmo che, a partire da un dataset $ D $, restituisce una espressione logica $ H $ che è esattamente l'\acs{lgg}.

\begin{algorithm}
	\caption{Algoritmo per ricavare \acs{lgg} da un insieme $ D $ di dati.}
	\begin{algorithmic}[1]
		\State {Input: data D.}
		\State {Output: logical expression H.}
		\State {x := first instance from D;}
		\State {H := x;}
		\While {instances left}
		\State {x := next instance from D;}
		\State {H := LGG(H,x) ; // e.g., LGG-Conj or LGG-Conj-ID}
		\EndWhile
		\State {return H}
	\end{algorithmic}
\end{algorithm}

\begin{algorithm}
	\caption{Algoritmo che effettua la congiunzione tra letterali.}
	\begin{algorithmic}[2]
		\State {Input : conjunctions x, y.}
		\State {Output : conjunction z.}
		\State {z := conjunction of all literals common to x and y;}
		\State {return z}
	\end{algorithmic}
\end{algorithm}

L'algoritmo comincia considerando il primo dato $x \in D$, ed essendo l'unico esempio finora considerato non potrà che asserire $H = x$.
Dopo di che, per ogni iterazione, considera un nuovo dato e valuta la nuova \acs{lgg} come visto in precedenza (rimuovendo letterali non coerenti se messi in congiunzione).

Terminata l'iterazione, restituisce H. \\
Plotkin, come tesi del suo PhD (1971) ha dimostrato che grazie alla struttura dello spazio delle ipotesi e all'unicità di \acs{lgg} per una coppia di elementi dati, è garantito che la H restituita dall'algoritmo è unica e non dipende dall'ordine in cui i dati vengono considerati.

\section{Introduzione di esempi negativi}
Se i concetti accettati fino a un certo punto coprono anche qualche esempio negativo la generalizzazione (costruita come fin'ora visto) sarà inconsistente. \\
Dovremo intervenire in modo da rendere consistenti i concetti. Non occorre intervenire su \acs{lgg}, bensì (come vedremo) sul margine \acs{lub}.
Riprendendo l'esempio introdotto nella sezione \ref{sec:ex} in cui avevamo i seguenti esempi positivi
\begin{enumerate}
	\item $
	Length = 3 (A1) \land Gills = no (B1) \land Beak=yes (C1) \land Teeth = many (D1) 
	$
	\item $
	Length = 4 \land Gills = no \land Beak=yes \land Teeth = many 
	$
	\item $
	Length = 3 \land Gills = no \land Beak=yes \land Teeth = few 
	$
\end{enumerate}
Che aveva portato ad avere la generalizzazione $Gills = no \land Beak=yes$.\\
Viene ora aggiunto il seguente esempio:
\begin{enumerate}
	\item $Length = 5 \land Gills = yes \land Beak = yes \land Teeth = many$
\end{enumerate}
Ci si accorge subito che il letterale $Beak = yes$ (da solo) non rende più accettabili alcune generalizzazioni prima possibili, e in particolare porta alla eliminazione della generalizzazione più alta $ TRUE $.\\
Come accennato occorre aggiornare l'upper border "abbassandolo", ovvero specificando maggiormente lo spazio delle ipotesi fermo restando l'accettazione degli esempi che possono risultare ancora validi. Questo viene fatto attraverso la procedura di ricerca della \emph{specializzazione minima di \acs{lub}}, con cui si aggiungono delle clausole che permettono di non coprire gli esempi negativi.
Nella Figura \ref{fig:introneg} si rappresenta in rosso l'esempio negativo, i percorsi in rosso rappresentano cammini che portano all'esempio negativo e che quindi non sono più corretti.

\begin{figure}
	\centering
	\includegraphics[width=1\linewidth]{./Images/IntroNegative}
	\caption{Introduzione di un esempio della classe negativa}
	\label{fig:introneg}
\end{figure}

Quindi solamente tre congiunzioni coprono tutti gli esempi positivi e nessun esempio negativo: $ Gills = no \land Beak=yes $ e $ Gills = no $.
È ora utile introdurre nel linguaggio quella che Flach chiamò \emph{disgiunzione interna}, che introduce la possibilità di attribuire una lista di valori ad una \emph{feature}, chiedendo così che quest'ultima assuma un valore tra quelli possibili.
Con riferimento al solito esempio, ed in particolare alle istanze 3 e 4, si potrà così ricavare la nuova clausola:
$$
Length = [3, 4] \land Gills = no \land Beak = yes \land Teeth = many
$$
che, considerato che la feature Teeth potrà assumere qualsiasi valore del suo dominio (gli esempi positivi potranno avere sia $Teeth = few$ e $Teeth = many$ ed è quindi inutile usare la disgiunzione interna) si avrà:
$$
Length = [3, 4] \land Gills = no \land Beak = yes
$$
Si nota come l'esempio negativo non è coperto da tale congiunzione. \\
Generalizzando potremmo mantenere $Length = [3, 4] \land Gills = no$, perché coprirebbe gli esempi positivi ma escluderebbe correttamente l'esempio negativo. \\
Guardando lo spazio delle ipotesi (Figura \ref{fig:introneg}) ci si accorge che esistono ipotesi interne tra \acs{lub} e \acs{glb}, vedasi Figura \ref{fig:verspace}, che sono comunque accettabili. Questo suggerisce che questa teoria è formata da diverse ipotesi, tutte valide e alternative fra di loro, contenute nello spazio delle ipotesi dei dati.

\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.75\linewidth]{Images/VersionSpace}
	\caption{Version space dell'esempio considerato.}
	\label{fig:verspace}
\end{figure}

Tale insieme di ipotesi è detto \emph{version space} ed è un insieme \emph{convesso}: si può descrivere una qualsiasi ipotesi in essa considerando le due ipotesi agli estremi (la più specifica e la più generale).
Vediamo ora la procedura che costruisce \acs{lgg} quando si utilizza la disgiunzione interna.

\begin{algorithm}
	\caption{Find least general conjunctive generalisation of
		two conjunctions, employing internal disjunction.}
	\begin{algorithmic}[1]
		\State {Input: conjunctions x,y;}
		\State {Output: conjunction z;}
		\State { z:= true;}
		\For {each feature f}
		\If {f=$v_x$ is a conjunct in x and f = $v_y$ is a conjunct in y} add f = Combine-ID($v_x$, $v_y$) to z
		\EndIf
		\EndFor
		\State {return z} \\
		\Comment {Combine-ID([set-values-x],[set-values-y])=[set-values-x UNION set-values-y]}
	\end{algorithmic}
\end{algorithm}

\section{Paths through the hypothesis space}

Si introdurrà ora un po' di notazione, facendo anche un ripasso di quanto visto in precedenza.\\
Un concetto è \emph{completo} se copre tutti gli esempi positivi.\\
Un concetto è \emph{consistente} se non copre nessun esempio negativo.\\
Il \emph{version space} è un insieme di tutti i concetti completi e consistenti. Quest'ultimo insieme è convesso\footnote{Dati due membri ($H1$, $H2$) dell'intero insieme delle ipotesi (insieme convesso), ciascun membro $H_x$ dell'insieme convesso che è meno generico di $H_1$ ($H_1$ $preccurlyeq$ $H_x$) e più generale i $H_2$ appartiene anch'esso all'insieme convesso.} e pienamente definito entro \acs{lub} e \acs{glb}.


Vediamo un esempio. A partire dal grafo, si sa essere possibile salire dal \acs{glb} mantenendo la validità dei concetti e incrementando la generalità. \\
Si seguirà ora la rappresentazione della Figura \ref{fig:expath} dandone una didascalia esaustiva.

\begin{figure}
	\centering
	\includegraphics[width=0.65\linewidth]{./Images/ExPath}
	\caption{Esempio di generalizzazione basato sel version set}
	\label{fig:expath}
\end{figure}

All'inizio si pone A, che è la descrizione completa dell'esempio p1 che quindi ricopre alla perfezione. Salendo di livello si trova l'esempio p2 e il concetto B, ottenuto tramite l'applicazione di disgiunzione interna sulla feature $ Lenght $ e che quindi copre gli esempi p1 e p2. Arrivando al concetto C, si raggiunge il cosiddetto \emph{Roc Heaven}, in quanto questo concetto riesce a coprire pienamente gli esempi positivi senza coprire alcun esempio negativo. \\
È possibile generalizzare ulteriormente escludendo (cioè considerando dato) l'attributo $ Gills $, raggiungendo così un'altra ipotesi egualmente valida.\\
Se si generalizza ancora (lasciando solamente l'attributo $ Beak = yes $) si otterrebbe però un concetto che copre anche l'esempio negativo precedentemente introdotto.

Tracciando un percorso attraverso lo spazio delle ipotesi si avrà una curva corrispondente alla curva di copertura: se un percorso (nel nostro esempio dato da A-B-C-D) include elementi del \emph{version space} (completi e consistenti) allora corrisponde alla curva di copertura passante per il \emph{Roc Heaven} e quindi è un percorso ottimale.

L'obbiettivo del concept learning può essere definito come la ricerca dell'insieme dei concetti che permettono di tracciare un percorso passante per il \emph{Roc Heaven}, percorso oltre il quale sconfiniamo dal version space.

\section{Dati non congiuntamente separabili}

Questa procedura non sempre funziona: talvolta può capitare di avere dati non congiuntamente separabili. In questo caso non si sarebbe in grado (usando il linguaggio così come definito con la sola congiunzione logica e la disgiunzione interna) di definire ipotesi valide (complete e consistenti): il version space risulterebbe così vuoto.\\
Un caso di questo tipo è proprio l'esempio sotto riportato, in cui si hanno 5 esempi positivi e altrettanti negativi:
\begin{description}
	\item[p1] $Lenght=3 \land Gills=no \land Beak=yes \land Teeth = many$
	\item[p2] $Lenght=4 \land Gills=no \land Beak=yes \land Teeth=many$
	\item[p3] $Lenght=3 \land Gills=no \land Beak=yes \land Teeth = few$
	\item[p4] $Lenght=5 \land Gills=no \land Beak=yes \land Teeth=many$
	\item[p5] $Lenght=5 \land Gills=no \land Beak=yes \land Teeth=few$
\end{description}
\begin{description}
	\item[n1] $Lenght=5 \land Gills=yes \land Beak=yes \land Teeth = many$
	\item[n2] $Lenght=4 \land Gills=yes \land Beak=yes \land Teeth=many$
	\item[n3] $Lenght=5 \land Gills=yes \land Beak=no \land Teeth = many$
	\item[n4] $Lenght=4 \land Gills=yes \land Beak=no \land Teeth=many$
	\item[n5] $Lenght=4 \land Gills=no \land Beak=yes \land Teeth=few$
\end{description}

Viene quindi introdotto l'Algoritmo \ref{alg:mgconsistent} che ha lo scopo di ottenere una \emph{Most General Consistent Specialisation}.
\begin{algorithm}
	\caption{Find most general consistent specialisations of a concept.}
	\begin{algorithmic}
		\State {\textbf{Input}: current concept C to update (upper boarder); negative examples N}
		\State {\textbf{Output}: set of concepts S}
		\If {C doesn't cover any element from N}
		\State {return C}
		\EndIf
		\State {$S \leftarrow \phi$}
		\For {each minimal specialisation $C'$ of $C$}
		\State { $ S \leftarrow S \bigcup MGConsistent(C', N) $ }
		\EndFor
		\State {return S}
		\Comment {Add a conjunct or eliminate a value in a disjunction; in particular the conjunct could be taken from	the clauses of the \acs{lgg} (low border)}
	\end{algorithmic}
	\label{alg:mgconsistent}
\end{algorithm}
\\Come ricavare la specializzazione $C'$ di $C$? Nell'algoritmo non è specificato ma una opzione iniziale potrebbe essere quella di cominciare da un singolo letterale di $C$ (con il vincolo di appartenere alla classe positiva, altrimenti si rappresenterebbe la classe negativa).\\ 
Un modo per ottenere ciò è considerare l'\acs{lgg}, il "minimal border" che copre tutti gli esempi positivi e nessun esempio negativo. Ovviamente non si è certi che \acs{lgg} sia consistente con gli esempi negativi: per questo, come si vede, si richiama ricorsivamente la procedura.
%Domanda su questo algoritmo: ha ancora senso parlare di LGG

\subsection{Esempio di applicazione dell'algoritmo MG Consistent}

\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.65\linewidth]{./Images/ExampleMGConsistent}
	\caption{Esempio di applicazione dell'Algoritmo \ref{alg:mgconsistent}.}
	\label{fig:exMGC}
\end{figure}

Se si considera ora l'esempio in figura \ref{fig:exMGC} in cui si denota un percorso nello spazio delle ipotesi, si nota che il concetto A copre solamente l'esempio p3, B copre l'esempio p5, C copre tutti gli esempi positivi tranne p2 e D ed E che sono \acs{lgg} di tutti e 5 gli esempi positivi ma che coprono anche l'esempio negativo n5. \\
Soffermandosi però sui concetti D ed E, si vuole evidenziare come questi due concetti occupino lo stesso punto nella curva di copertura: generalizzando D in E (rimuovendo cioè $ Beak = yes $) la curva non cambia e questo significa che nel contesto del concetto E la condizione $ Beak = yes $ è in realtà implicita (perché segue da $ Gills = no $ e quindi ogni volta che abbiamo quest'ultima avremo anche la prima).

Un concetto che include implicitamente delle condizioni è detto concetto chiuso. Tuttavia, tutto ciò non significa che D ed E sono logicamente equivalenti: esistono (probabilmente) istanze nel dominio che sono coperte da E ma non da D, ma queste istanze non sono presenti nel nostro insieme di dati. Non potendo prendere in considerazioni queste istanze, D ed E sono per noi indistinguibili. \\
Oltre quanto detto, il libro e le slide si soffermano parecchio sulla differenza di questi due punti, ma noi sorvoleremo\footnote{Speriamo anche in sede di esame}. \\
In generale nei casi come quello dell'esempio che stiamo portando avanti in cui non siamo in grado di raggiungere il Roc Heaven perchè non esiste modo di trovare un concetto che sia contemporaneamente completo e consistente, ci si attiene a considerare il concetto più generale, rinunciando alla consistenza ma garantendo la completezza se si ritiene che questa assunzione porti nel peggiore dei casi a danni minore e viceversa (meglio un falso positivo che un true negative).

\section{Clausole di Horn}

È stato finora utilizzato un certo tipo di linguaggio definito all'inizio del Capitolo. Si introduce ora una nuova componente: le \emph{clausole di Horn}.\\
Una \emph{clausola di Horn} è un'implicazione della forma $A \rightarrow B$ dove $ A $ è una congiunzione di letterali e $ B $ è un letterale (così come definito e utilizzato finora).\\
La clausola $ A \rightarrow B $ equivale a $ \lnot A \lor B $.
Da qui in poi si tratteranno espressioni logiche in \emph{Forma Normale Congiuntiva} che altro non sono che rappresentate dalla congiunzione di letterali\footnote{Nel caso in esame si tratta di disgiunzioni ottenute a partire da implicazioni, ovvero le clausole di Horn.}.\\
Già, ma cosa cambia? Quanto è stato fatto finora è, a partire da un insieme di esempi, apprendere una teoria che permette di coprire tutti gli esempi positivi: se un esempio positivo non veniva coperto dalla teoria attuale occorreva generalizzarla per includerlo mentre, qualora la teoria includeva un esempio della classe negativa, occorreva escluderlo specializzando l'ipotesi.\\
Ora, con le clausole di Horn, si hanno più opzioni:
\begin{itemize}
	\item nel caso in cui un esempio non sia coperto dalla teoria attuale (ora descritta tramite la congiunzione di letterali) possiamo:
	\begin{itemize}
		\item applicare la generalizzazione (come appena ricordato);
		\item eliminare le clausole che violano l'esempio (ad esempio una clausola del tipo $ A \rightarrow B $ con $ A $ è vera e $ B $ è falsa);
	\end{itemize}
	\item nel caso in cui un esempio negativo sia erroneamente incluso nella teoria possiamo:
	\begin{itemize}
		\item effettuare una specializzazione tramite l'aggiunta di più clausole (come appena ricordato);
		\item supponendo che l'esempio negativo sia coperto da un concetto del tipo $A \land \lnot B$, si aggiunge all'insieme delle \emph{most general conjunctive hypothesis} la clausola opposta $\lnot (A \land \lnot B) = \text{(De Morgan)} (\lnot A \lor B) = \text{(Tavole di verità)} (A \leftarrow B)$. (l'algoritmo che vedremo a breve farà uso di questa osservazione);
		\item supponendo che l'esempio sia coperto dal concetto $A \land B$, si aggiunge all'insieme delle \emph{most general conjunctive hypothesis} la clausola opposta $ \lnot (A \land B) = \text{(De Morgan)} (\lnot A \lor \lnot B) = \text{(Tavole di verità)} (A \leftarrow \lnot B) $.
	\end{itemize}
\end{itemize}

Si introduce quindi il nuovo Algoritmo \ref{alg:horn} che fa uso delle clausole di Horn per ottenere concetti che includono tutti gli esempi positivi e che escludono tutti gli esempi negativi. \\
Nella situazione in cui opera, questo algoritmo apprende una teoria $ h $ basata sugli esempi della classe positiva. Il training set contiene però esempi di entrambe le classi: accade spesso nel machine learning che si ricorra a conoscenza pregressa\footnote{Questo ramo del machine learning rientra nel campo dell'\emph{Active Learning}, in cui si assume esistere una "entità esterna" che interagisce con l'algoritmo al fine di modificare il training set}, che assumiamo essere il nuovo background di verità.
Si assume quindi di avere già una teoria valida per la classe positiva, e si indicherà con l'espressione booleana $ f $. 
Inoltre, nell'algoritmo avremo si hanno due\emph{oracoli}, ovvero una "presenza" in grado di rispondere a delle questioni in modo corretto e in un tempo finito:
\begin{itemize}
	\item il primo è Mb, che risponde alla classificazione di un esempio sulla base della teoria h costruita.
	\item il secondo è Eq, che risponde "$true$" qualora la teoria h è logicamente equivalente all'espressione booleana f.
\end{itemize}
L'algoritmo utilizza una operazione di intersezione tra un esempio $ x $ e un altro esempio $ s \in S $ (insieme degli esempi negativi). Questa operazione ritorna due insiemi:
\begin{itemize}
	\item un insieme di verità composto da tutti i letterali che sono verificati sia per x che per s.
	\item un insieme di falsità composto da tutti i letterali che sono falsi per x o per s
\end{itemize}

\begin{algorithm}
	\caption{Learn a conjunction of Horn clauses from membership and equivalence oracles.}
	\begin{algorithmic}[1]
		\State {\textbf{Input}: equivalence oracle Eq; membership oracle Mb.}
		\State {\textbf{Output}: Horn theory h equivalent to target formula f.}
		\State {$h \rightarrow true;$}
		\Comment {conjuction of Horn clauses, initially empty}
		\State {$S \rightarrow \phi;$}
		\Comment {a list of negative examples, initially empty}
		\While {Eq(h) returns counter-example x}
		\If {x violates at least one clause of h}
		\Comment {x is a false negative}
		\State {generalize h by removing every clause that x violates}
		\Else 
		\Comment {x is a false positive}
		\State {find the first negative example $s \in S$ such that $z = s \cap x has fewer true literals than s, and Mb(z) labels it as a negative;$}
		\State {if such an example exists then replace s in S with z, else append x to the end of S;}
		\State {$h \rightarrow true$}
		\For {all $s \in S$}
		\Comment {rebuild h from S}
		\State {$p \rightarrow$ the conjunction of literals true in s;}
		\State {$Q \rightarrow$ the set of literals \textsubscript{false} in s;}
		\For {all $q \in Q$}
		\State {$h \rightarrow h \land (p \leftarrow q)$;}
		\EndFor
		\EndFor
		\EndIf
		\EndWhile \\
		\Return {h}
	\end{algorithmic}
	\label{alg:horn}
\end{algorithm}

La terminazione dell'algoritmo è garantita in un tempo polinomiale nel numero di features e nel numero di clausole. \\
Si nota inoltre che l'algoritmo è in accordo con quello che si definisce \emph{\acs{PAC}-learning model}, ovvero un modello appreso dagli esempi che sappiamo essere probabilmente corretto (con un certo livello di fiducia) data la distribuzione dei valori nel dominio.

\section{ILP - Inductive Logic Programming}

Per anni (circa gli ultimi 20) si è considerato inefficiente (soprattutto per le grosse moli di dati) l'approccio logico alla programmazione, tanto che la professoressa che tiene questo corso era quasi decisa a non trattare più questa parte di programma.\\
Se non fosse che, per la fortuna della nostra conoscenza, partecipò ad una conferenza in cui un ricercatore italiano presso la California University in Database e Machine Learning  he risponde al nome di Carlo Zaniolo, presentò la sua ricerca sulla sorprendente potenza di linguaggi come \emph{Datalog} e \emph{Prolog} nel gestire algoritmi di Machine Learning. Questo nuovo approccio risponde al nome di \emph{Inductive Logic Programming}. \\
Mentre nel linguaggio visto prima si utilizzano solamente proposizioni booleane per esprimere i concetti, nel linguaggio logico si utilizzano predicati della logica del prim'ordine che permettono di esprimere letterali più complessi utilizzando termini composti anche da simboli funzionali e da costanti. I vantaggi introdotti saranno:
\begin{itemize}
	\item poter utilizzare singoli termini per riferirsi agli oggetti;
	\item poter descrivere la struttura di un oggetto usando i predicati;
	\item poter introdurre variabili per riferirsi ad oggetti non specifici. Ad esempio, a partire da $BodyPart(x, PairOf(Gill))$ si può ricavare quello che prima erano le operazioni di generalizzazione e specializzazione e che ora chiameremo:
	\begin{description}
		\item [anti-unification] $BodyPart(x, PairOf(y))$
		\item [unification] $BodyPart(Dolphin42, PairOf(Gill))$
	\end{description}
\end{itemize}

Questo approccio permette quindi di utilizzare ad un livello più alto un linguaggio che fa uso della logica del prim'ordine (che è, come appena visto, più completo e complesso), e ad un livello più basso un linguaggio proposizionale semplice in grado di rispondere in tempi molto veloci alle query che gli vengono sottoposte.

\section{Ridurre l'insieme di esempi}

Si vuole, in buona sostanza, avere un modo di escludere alcuni esempi fermo restando la correttezza di ciò che si intende apprendere e migliorando addirittura la conoscenza? \\
La risposta è ovviamente affermativa, e in parte è quello che già si fa nell'Algoritmo \ref{alg:horn}.\\
Supponendo di avere i seguenti esempi:
\begin{description}
	\item [m] $ManyTeeth \land \lnot Gills \land \lnot Short \land \lnot Beak$;
	\item [g] $ \lnot ManyTeeth \land Gills \land \lnot Short \land \lnot Beak$;
	\item [s] $ \lnot ManyTeeth \land \lnot Gills \land Short \land \lnot Beak$;
	\item [b] $ \lnot ManyTeeth \land \lnot Gills \land Short \land Beak$.
\end{description}
è chiaro che avremo $ 16 $ possibili sottoinsiemi di $ \{m, g, s, b\} $ e ciascuno di essi può essere rappresentato da un concetto: per ogni esempio che vogliamo escludere, aggiungiamo al concetto (tramite congiunzione) la negazione.

%Non l'ho mica capito... meglio chiedere (slide 55 capitolo 4)