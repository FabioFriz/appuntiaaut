\chapter{Feature}\label{ch:features}\index{feature}

In questo capitolo si studieranno e confronteranno i diversi tipi di feature, in italiano traducibili con il termine attributi, identificandone caratteristiche e proprietà, ma anche le misure statistiche significative in base alla loro tipologia e al rispettivo dominio. Questo rientra nella fase esplorativa di un dataset e delle feature dei dati in esso contenuti, fase che si può considerare preliminare rispetto alle analisi vere e proprie che si andranno a fare sui dati veri e propri. La seconda parte del capitolo sarà dedicata invece alle operazioni necessarie per trasformare le feature in una forma che si presti meglio agli scopi della grande varietà di algoritmi di apprendimento automatico.

\section{Tipi di feature}

Le feature vengono definite come un mapping
\begin{equation}
\textit{f}_{\textit{i}} : \mathscr{X} \rightarrow \mathscr{F}_{\textit{i}} \nonumber
\end{equation}
fra lo spazio delle istanze $ \mathscr{X} $ e il dominio della feature $ \mathscr{F}_{\textit{i}} $; si possono caratterizzare in base al loro dominio e alle operazioni che permettono di fare. Possono essere distinte principalmente in base a:
\begin{description}
	\item[Dominio] I valori di una feature possono appartenere al dominio dei numeri interi o dei numeri reali, oppure fare parte di insiemi infiniti o finiti.
	\item[Operazioni possibili] Non si intendono solo le operazioni consentite dal dominio, ma anche quelle effettivamente sensate rispetto al contesto e al significato dell'attributo\footnote{Ha senso calcolare la media dell'età delle persone, ma non ha senso calcolare la media dei numeri civici dei loro indirizzi, sebbene in entrambi i casi il dominio sia quello degli interi.}.
\end{description}

La forma in cui si presentano le feature può non essere sempre quella migliore per gli algoritmi di apprendimento automatico, perciò si rendono necessarie operazioni di trasformazione come ad esempio la normalizzazione o la discretizzazione per cambiare il dominio dei valori. Di queste operazioni si parlerà in modo approfondito nella seconda parte di questo capitolo.

\subsection{Statistiche sulle feature}

Le misure statistiche possibili sulle feature possono essere raggruppate in tre macrocategorie:
\begin{description}
	\item[statistiche di tendenza centrale] Forniscono informazioni sul comportamento atteso dei valori della feature. Sono:
	\begin{itemize}
		\item media;
		\item mediana;
		\item moda.
	\end{itemize}
	\item[statistiche di dispersione] Forniscono informazioni su quanto i dati possano variare rispetto al comportamento atteso. Sono:
	\begin{itemize}
		\item varianza;
		\item deviazione standard;
		\item range;
		\item percentile.
	\end{itemize}
	\item[statistiche di forma] Forniscono informazioni sulla forma che assumono i valori della feature. Sono:
	\begin{itemize}
		\item asimmetria (a.k.a. \textit{skewness});
		\item curtosi.
	\end{itemize}
\end{description}

\begin{table}
	\centering
	\begin{tabular}{cccc}
		\toprule
		\multirow{2}{*}{Feature} & \multicolumn{3}{c}{Operazioni statistiche} \\% \cline{2-4}
		& Moda & Mediana & Media \\
		\midrule
		Categoriche & \checkmark & & \\
		Ordinali & \checkmark & \checkmark & \\
		Quantitative & \checkmark & \checkmark & \checkmark \\
		\bottomrule
	\end{tabular}
	\caption{Operazioni statistiche di tendenza centrale ammesse e non sui tre tipi diversi di feature.}
	\label{tab:op_centr_stats}
\end{table}

Per quanto riguarda le operazioni statistiche di tendenza centrale, queste non sono applicabili liberamente a tutti i tipi di feature, in particolare facendo riferimento alla tabella \ref{tab:op_centr_stats} si osserva che:
\begin{itemize}
	\item a feature puramente categoriche è possibile applicare solo la moda, trovare quindi la categoria più frequente, ed in assenza di valori numerici o anche solo di un ordinamento è impossibile calcolare rispettivamente la media e la mediana;
	\item a feature ordinali è possibile applicare l'operazione della mediana, in aggiunta alla moda, poiché l'ordinamento delle categorie permette di identificare quella che si colloca a metà del ranking;
	\item a feature quantitative, numeriche e quindi con un'unità di misura di riferimento, è possibile applicare anche la media.
\end{itemize}

L'operazione della media inoltre può variare in base al contesto e al dominio, dipendendo dalla scala dell'attributo quantitativo oggetto dell'operazione:

\begin{description}
	\item[media aritmetica] si utilizza quando la scala è lineare;
	\item[media armonica] si utilizza quando la scala è reciproca;
	\item[media geometrica] si utilizza quando la scala è moltiplicativa.
\end{description}

Un'ulteriore proprietà infine lega la media e la mediana, indipendentemente dalla distribuzione dei valori. Partendo dalla disuguaglianza di Čebyšëv nella sua formulazione generale:
\begin{equation}
\textit{P}(|\textit{X} - \mu| \ge \textit{k}\sigma) \le \frac{1}{\textit{k}^{2}} \nonumber
\end{equation}

scegliendo $ \textit{k} = \sqrt{2} $ è possibile affermare che la distanza in valore assoluto fra la media e la mediana non è mai maggiore di $ \sqrt{2}\sigma $, dove $ \sigma $ si ricorda essere la deviazione standard, per cui almeno metà dei valori si trova nell'intervallo:
\begin{equation}
(\mu - \sqrt{2}\sigma, \mu + \sqrt{2}\sigma) \nonumber
\end{equation}

\subsubsection{Statistiche di forma}
\index{asimmetria}
\index{curtosi}

Le statistiche di forma descrivono se la distribuzione dei dati è sbilanciata o meno, ovvero la simmetria o asimmetria dovuta alla presenza di picchi nella distribuzione dei valori; si basano sul concetto di \emph{momento centrale di ordine $ \textit{k} $}:
\begin{equation}
\textit{m}_{\textit{k}} = \dfrac{1}{\textit{n}} \sum_{\textit{i} = 1}^{\textit{n}} (\textit{x}_{\textit{i}} - \mu)^{\textit{k}} \nonumber
\end{equation}
Con $ \textit{k} = 1 $ il risultato è sempre nullo per definizione stessa di media\footnote{Il primo momento centrale è la deviazione media dalla media che deve essere quindi pari a $ 0 $ poiché la somma delle componenti positive si annulla con la somma delle componenti negative, salvo violare la definizione stessa di media.}; con $ \textit{k} = 2 $ si ottiene la formula della varianza e il risultato è sempre positivo\footnote{Si ricorda che la varianza è la somma del quadrato degli scarti, per cui la somma di soli valori positivi non può che dare un risultato anch'esso positivo.}.

Il rapporto fra il momento centrale di ordine $ \textit{k} = 3 $ e $ \sigma^{3} $, il cubo della deviazione standard, definisce il \emph{coefficiente di asimmetria}:
\begin{equation}
\gamma_{1} = \dfrac{\textit{m}_{3}}{\sigma^{3}} \nonumber
\end{equation}
Tale coefficiente di asimmetria, noto anche come \textit{Pearson's moment coefficient of skewness}, può assumere valori positivi (coda più lunga a destra) o negativi (coda più lunga a sinistra).

Il rapporto fra il momento centrale di ordine $ \textit{k} = 4 $ e $ \sigma^{4} $ definisce l'\emph{indice di curtosi}:
\begin{equation}
\beta_{2} = \dfrac{\textit{m}_{4}}{\sigma^{4}} \nonumber
\end{equation}
Poiché per una distribuzione normale si ottiene $ \beta_{2} = 3 $, è stato definito il \emph{coefficiente di curtosi}:
\begin{equation}
\gamma_{2} = \beta_{2} - 3 \nonumber
\end{equation}
Per valori positivi di tale coefficiente si hanno picchi più alti ("appuntiti") rispetto ad una distribuzione normale, per valori negativi si hanno meno picchi ("appiattiti"), mentre quando vale esattamente $ 0 $ si ha proprio la distribuzione normale.

\subsection{Feature categoriche, ordinali, quantitative}

Sulla base delle statistiche fino ad ora presentate è possibile distinguere principalmente tre tipi di feature:
\begin{description}
	\item[feature quantitative] sono le più generali e si distinguono per avere una scala numerica significativa che permette di fare un numero maggiore di operazioni matematiche, si dicono anche feature a valori continui;
	\item[feature ordinali] non hanno una scala numerica associata, bensì solo un ordinamento, quindi non è possibile fare operazioni aritmetiche su di esse ma solo alcune statistiche (calcolare moda e mediana, ma non la media, ad esempio);
	\item[feature categoriche] sono le più semplici, non c'è una scala numerica e viene meno anche l'ordinamento, un caso particolare sono le feature booleane che possono assumere solo due valori (vero o falso).
\end{description}

La tabella \ref{tab:feature_stats} è stata ottenuta partendo dalla tabella \ref{tab:op_centr_stats}; sulla base delle proprietà delle feature, ovvero avere una scala numerica associata o solo un ordine, si riassumono per le statistiche di tendenza, di dispersione e di forma quali operazioni sono possibili. Poiché nella tabella le feature sono ordinate dalle meno specifiche alle più specifiche, ogni tipo eredita le operazioni ammesse per quello precedente (più specifico). Ad esempio: sulle feature categorighe è possibile calcolare solo la moda fra le tre statistiche di tendenza, mentre le feature ordinali che introducono un ordinamento rendono possibile oltre al calcolo della moda anche l'operazione della mediana.

\begin{table}
	\centering
	\begin{tabular}{cccccc}
		\toprule
		\textit{Feature} & \textit{Ordine} & \textit{Scala} & \textit{Tendenza} & \textit{Dispersione} & \textit{Forma} \\
		\midrule
		Categoriche & & & Moda & & \\
		Ordinali & \checkmark & & Mediana & Quantili & \\
		Quantitative & \checkmark & \checkmark & Media & \parbox{3.5cm}{Range, range interquartile, varianza, deviazione standard} & \parbox{2cm}{Asimmetria, curtosi} \\
		\bottomrule
	\end{tabular}
\caption{Proprietà dei tre tipi diversi di feature e relative operazioni statistiche ammesse.}
\label{tab:feature_stats}
\end{table}

\subsubsection{Feature diverse per modelli diversi}

La necessità di parlare di come trasformare le feature da un tipo ad un altro nasce dalle preferenze dei modelli per determinati tipi di feature. Ad esempio i modelli probabilisti (naive Bayes, per citarne uno) calcolano delle probabilità sulle feature, preferiscono quindi quelle di tipo categorico perché sono discrete e non continue; i modelli ad albero ignorano invece la proprietà quantitativa, nei test che vengono fatti all'interno dei nodi dell'albero decisionale si considerano quindi come ordinali le feature quantitative operando semplici confronti ($ > $, $ < $, etc...). I modelli geometrici infine prediligono feature quantitative poiché si basano su funzioni per il calcolo di distanze, per cui feature di tipo ordinale o categorico richiedono delle trasformazioni affinché possano essere utilizzate a tale scopo.

\subsection{Feature strutturate}

Utilizzando come esempio l'ambito dell'analisi del testo, si è soliti usare come feature le parole e la frequenza con cui occorrono. Tuttavia potrebbe essere necessario includere nuove feature come parole composte, la presenza di parole o intere frasi in un'altra lingua, relazioni fra parti del testo o fra testi diversi, e così via.

Le feature strutturate possono essere paragonate a delle interrogazioni che vanno a verificare se determinate proprietà più o meno complesse sono verificate, in questo modo è possibile strutturare nuove feature nelle fasi preliminari di preprocessing. Ciononostante si deve tenere in conto la possibile esplosione combinatoria di questo tipo di feature, aspetto che va bilanciato con l'espressività effettivamente necessaria agli algoritmi di machine learning per funzionare correttamente ed efficacemente.

\section{Trasformazione delle feature}\index{feature!trasformazione}

Sovente i dati di cui si dispone non si presentano nella forma più adatta per il tipo di elaborazione che si svolge con gli algoritmi di machine learning, per questo motivo è quasi sempre richiesto un preprocessamento volto ad effettuare delle trasformazioni di questi dati affinché possano costituire un input migliore per permettere agli algoritmi di apprendimento di estrapolare maggiore conoscenza, o più in generale risolvere meglio il task prefissato.

Le trasformazioni alle quali si è fatto cenno poc'anzi sono numerose in quantità e in natura, il loro scopo è generalmente quello di \emph{variare l'espressività} delle feature, aumentando o riducendo la granularità, o risoluzione, dei dati in base al contesto e alle necessità. I primi esempi a riguardo sono già stati fatti nell'introduzione a questi argomenti nelle sottosezioni \ref{ssec:intro_feature} e \ref{ssec:intro_feature_transf}. Più in generale potrebbe non essere necessario utilizzare dei dati ad alta risoluzione come ad esempio i numero reali, bensì potrebbe considerarsi sufficiente considerare come uniformi determinati intervalli di valori svolgendo una \emph{discretizzazione}. Portando all'estremo questa trasformazione e dividendo i valori reali in due soli intervalli distinti da una soglia (\emph{thresholding}) si arriverebbe alla \emph{binarizzazione}.

Dagli esempi succitati si può notare come le operazioni di trasformazione delle feature siano \emph{irreversibili} e con \emph{perdita di informazione}; vi sono comunque operazioni che al contrario permettono di aumentare la risoluzione dei dati, quindi aumentare la loro quantità di informazione: si parla in questo caso di \emph{calibrazione}.

\begin{table}
	\centering
	\begin{tabular}{ccccc}
		\toprule
		$ \downarrow $ to, from $ \rightarrow $ & \textit{Quantitative} & \textit{Ordinal} & \textit{Categorical} & \textit{Boolean} \\ 
		\midrule 
		\textit{Quantitative} & Normalisation & Calibration & Calibration & Calibration \\ 
		\textit{Ordinal} & Discretisation & Ordering & Ordering & Ordering \\ 
		\textit{Categorical} & Discretisation & Unordering & Grouping & \\
		\textit{Boolean} & Thresholding & Thresholding & Binarisation & \\
		\bottomrule
	\end{tabular}
	\caption{Tabella riassuntiva delle operazioni possibili per la trasformazione delle feature da una tipologia all'altra.}
	\label{tab:features_transformations}
\end{table}

La tabella \ref{tab:features_transformations} riassume le operazioni possibili per trasformare le feature che verranno affrontate e approfondite in questa sezione; nella prima riga viene indicata la tipologia di feature dalla quale si parte, nella prima colonna invece si trova il tipo di feature che si desidera ottenere.

\subsection{Discretizzazione e thresholding}
\index{discretizzazione}
\index{discretizzazione!thresholding}
\index{thresholding|see {discretizzazione, thresholding}}

Il \emph{thresholding}, così come suggerisce il nome stesso, consiste nell'introduzione di una soglia sulla base della quale trasformare una feature di tipo quantitativo (numeri reali) o ordinale (numeri naturali) in una di tipo booleano. Questa operazione può essere formalizzata come segue:
\begin{equation}
\textit{T} : \mathbb{R} \rightarrow \textit{true},\ \textit{false} \nonumber
\end{equation}

Scelta una soglia $ \textit{t} \in \mathbb{R} $ si ottiene una feature $ \mathscr{F}_{\textit{t}} $ tale per cui lo spazio delle istanze $ \mathscr{X} $ viene mappato sui due valori $ {\textit{true},\ \textit{false}} $ se esiste una funzione $ \textit{f} : \mathscr{X} \rightarrow \mathbb{R} $ per la quale vale che:
\begin{itemize}
	\item $ \mathscr{F}_{\textit{t}}(\textbf{x}) = \textit{true}\ \text{se}\ \textit{f}(\textbf{x}) \ge \textit{t} $;
	\item $ \mathscr{F}_{\textit{t}}(\textbf{x}) = \textit{false}\ \text{se}\ \textit{f}(\textbf{x}) < \textit{t} $.
\end{itemize}

La scelta della soglia diventa quindi il passaggio fondamentale di questa operazione in base alla quale è possibile determinare se si utilizza un approccio supervisionato o non supervisionato. Nel primo caso le istanze del dataset devono disporre di etichette sulle quale basare la ricerca della soglia migliore per dividere i dati; nel secondo caso, in assenza di etichette, la scelta della soglia viene basata su statistiche di tendenza centrale.

Il thresholding prevede la scelta di una soglia per dividere in due soli intervalli i dati che compongono il dataset, ma questa operazione può essere generalizzata per identificare un numero di soglie maggiore per ottenere un numero di intervalli superiore a due. In questo caso più generale si parla di \emph{discretizzazione}. Se i dati di cui si dispone sono imprecisi, o qualora la loro variabilità causi rumore, sostituire questi valori con un rappresentante (l'intervallo in cui ricadono) attraverso la discretizzazione si rivela un metodo efficace per mitigare il problema.

\begin{table}
	\centering
	\begin{tabular}{cc}
		\toprule
		\textit{Unsupervised}	& \textit{Supervised} \\ 
		\midrule 
		Equal-width				& Top-down (divisive) \\ 
		Equal-frequency			& Bottom-up (agglomerative) \\
		\bottomrule
	\end{tabular}
	\caption{Tabella riassuntiva delle possibili operazioni di discretizzazione in base alla tipologia di apprendimento.}
	\label{tab:discretisation}
\end{table}

La discretizzazione può essere declinata in diversi modi in base alla tipologia di apprendimento che si utilizza, supervisionato o meno; la differenza principale fra questi due diversi approcci è che nel primo caso il numero di intervalli viene determinato dai dati, mentre nel secondo caso va scelto a priori. La tabella \ref{tab:discretisation} riassume queste varianti.

\subsubsection{Equal-width discretisation}

Prendendo come esempio un dataset le cui istanze sono caratterizzate da $ \textit{d} $ feature distinte, se si vuole applicare la discretizzazione alla $ \textit{i} $-esima feature per ottenere $ \textit{k} $ intervalli distinti (ricordando che il valore $ \textit{k} $ va scelto a priori) vanno identificati il valore massimo e il valore minimo per la feature interessata al fine di definire gli estremi del'intervallo da suddividere. Ogni intervallo avrà quindi la stessa dimensione $ \frac{\max(\textit{f}_{\textit{i}}) - \min(\textit{f}_{\textit{i}})}{\textit{k}} $ e sarà rappresentato dal suo stesso punto medio, questo diventa quindi il nuovo valore per la $ \textit{i} $-esima feature di ogni istanza del dataset che ricade nel suo intervallo.

Il maggiore vantaggio di questa soluzione è sicuramente la sua semplicità e facilità di applicazione, tuttavia sono evidenti due non indifferenti controindicazioni:
\begin{itemize}
	\item non viene tenuto conto della distribuzione delle istanze del dataset, motivo per cui una discretizzazione omogenea non produce intervalli che dividono le istanze in modo coerente;
	\item i punti che si discostano maggiormente dalla media (\textit{outliers}) sono fonte di rumore e influiscono negativamente nella selezione delle soglie.
\end{itemize}

\begin{figure}
\centering
\includegraphics[width=1\textwidth]{Images/Discretisations}
\caption{Esempio di applicazione di tre diversi tipi di discretizzazione ad un dataset composto da punti di quattro colori diversi.}
\label{fig:discretisation_example}
\end{figure}

Nella figura \ref{fig:discretisation_example} viene riportato un dataset di esempio composto da una serie di punti distribuiti in base al loro colore\footnote{Agli estremi sinistro e destro sono presenti due outlier, rispettivamente blu e rosso, che si rivelano utili per l'approccio equal-width in quanto determinano i due estremi dall'intervallo da discretizzare.}, a questo dataset sono stati applicati tre diversi approcci di discretizzazione. Applicando la equal-width discretisation scegliendo $ \textit{k} = 4 $ vengono calcolate tre soglie sulla base del valore minimo e del valore massimo della feature rappresentata sull'asse delle ascisse e facilmente individuabile rispettivamente con il punto blu e il punto rosso agli estremi del grafico. Come si può vedere gli effetti dei due svantaggi sopra citati si manifestano e le tre soglie sono visibilmente sfasate, tant'è che punti di colori diversi ricadono nello stesso intervallo e punti dello stesso colore che invece dovrebbero ricadere nello stesso intervallo vengono separati.

\subsubsection{Equal-frequency discretisation}

Per cercare di mitigare i problemi di cui sopra si adotta una soluzione che vada a tenere in considerazione la distribuzione dei dati nel dataset, si parla quindi di discretizzazione equal-frequency. Fissando sempre a priori il valore di $ \textit{k} $, ogni intervallo rappresenta un $ \frac{100}{\textit{k}} $ percentile del dataset e di conseguenza ognuno contiene circa lo stesso numero di istanze.

Questo approccio permette di ottenere intervalli che possono andare ad adattarsi meglio alla distribuzione dei valori della feature oggetto dell'operazione di discretizzazione; il miglioramento si può notare anche nel rispettivo grafico nella figura \ref{fig:discretisation_example} dove le soglie dividono notevolmente meglio i punti di colori diversi.

\subsubsection{Top-down discretisation}

In questo approccio alla discretizzazione, così come nel successivo, si può utilizzare l'apprendimento supervisionato e sfruttare l'informazione contenuta nelle etichette associate alle istanze che compongono il dataset. La top-down discretisation parte da un ordinamento delle istanze sulla base del valore della feature che si desidera discretizzare, successivamente va a cercare i punti migliori dove introdurre le soglie e ottenere intervalli il più possibile omogenei rispetto all'etichetta, ovvero intervalli che contengano prevalentemente istanze appartenenti alla stessa classe.

Con l'adozione di algoritmi di apprendimento supervisionato non è più necessario specificare a monte il numero $ \textit{k} $ di intervalli, questo viene determinato dall'algoritmo stesso sulla base di una funzione per il calcolo dell'information gain (ricavato dall'entropia) o il coefficiente di Gini (Gini index), determinando quindi quando e dove vale ancora la pena introdurre una nuova soglia; nel caso in cui non fosse più conveniente dividere l'intervallo in due più piccoli, la ricorsione viene bloccata.

Una traccia molto generica dell'approccio algoritmico è riportata nell'algoritmo \ref{lst:recursivePartition}. L'input è costituito dal dataset $ D $, la feature da discretizzare $ f $ e la funzione per calcolare il punteggio degli intervalli $ Q $. Sia la funzione $ Q $ che la funzione \lstinline|stop()| sono volutamente generiche, poiché si possono scegliere diverse soluzioni per la loro implementazione come suggerito nel paragrafo precedente.

L'algoritmo ricorsivo quindi verifica in primo luogo se la funzione \lstinline|stop()| ritiene vantaggioso introdurre un'ulteriore soglia ed in caso non lo sia viene restituito l'insieme vuoto. La nuova soglia $ t $ viene calcolata dalla generica funzione \lstinline|bestSplit($ D $, $ f $, $ Q $)|, un'implementazione del calcolo dell'information gain o del coefficiente di Gini. Sulle due nuove partizioni ottenute viene invocato ricorsivamente l'algoritmo per verificare se l'intervallo può essere ulteriormente diviso o meno. Alla fine viene restituito l'insieme di tutte le soglie trovate.

\begin{lstlisting}[caption={Pseudocodice di un generico algoritmo per la discretizzazione top-down di una feature.}, label={lst:recursivePartition}]
recursivePartition($ D $, $ f $, $ Q $) 
	if stop() return $ \emptyset $
	$ t $ $ \leftarrow $ bestSplit($ D $, $ f $, $ Q $)
	$ D_{l} $, $ D_{r} $ $ \leftarrow $ binrySplit($ D $, $ t $)
	$ T_{l} $ $ \leftarrow $ recursivePartition($ D_{l} $, $ f $, $ Q $)
	$ T_{r} $ $ \leftarrow $ recursivePartition($ D_{r} $, $ f $, $ Q $)
	return $ T_{l} \cup {t} \cup T_{r} $
\end{lstlisting}

\subsubsection{Bottom-up discretisation}

Analogamente all'approccio precedente anche in questo caso viene sfruttato l'apprendimento supervisionato e l'informazione contenuta nelle etichette associate alle istanze che compongono il dataset; una precondizione necessaria, di nuovo, è l'ordinamento delle istanze rispetto al valore della feature che si desidera andare a discretizzare; anche il numero di intervalli è di nuovo determinato dall'algoritmo e non va specificato (e quindi conosciuto) a priori.

Nella discretizzazione top-down si partiva dal dataset intero e si cominciavano ad introdurre soglie per dividerlo in intervalli, in modo diametralmente opposto l'approccio bottom-up parte da una situazione in cui ogni istanza del dataset rappresenta un intervallo a sé stante e si procede ad unire intervalli contigui laddove conveniente finché un criterio di stop non faccia terminare il processo nel momento in cui non v'è più un vantaggio nel procedere con l'unione degli intervalli.

Il punto chiave è il criterio con il quale si decide se due intervalli contigui è conveniente unirli o meno. Un metodo diffuso è il test di $ \chi^{2} $ con il quale è possibile determinare se le frequenze delle classi degli esempi presenti nei due intervalli sono sufficientemente simili o troppo diverse. Il test di $ \chi^{2} $ viene fatto su ogni coppia di intervalli contigui, la coppia che risulta avere la distribuzione delle classi più simile fra loro diventa la prima candidata per essere unita in un unico intervallo.

L'algoritmo \ref{lst:agglomerativeMerge} non ha un approccio ricorsivo, bensì iterativo. Ad ogni iterazione, se il criterio di stop definito in modo generico con \lstinline|stop()| lo consente, si va a calcolare quali sono i due intervalli contigui che conviene maggiormente unire. Alla sua terminazione questo algoritmo può restituire l'insieme delle soglie che dividono gli intervalli rimasti.

\begin{lstlisting}[caption={Pseudocodice di un generico algoritmo per la discretizzazione bottom-up di una feature.}, label={lst:agglomerativeMerge}]
agglomerativeMerge($ D $, $ f $, $ Q $) 
	$ T $ $ \leftarrow $ initThresholds($ D $)
	$ T $ $ \leftarrow $ mergePure($ T $) // Ottimizzazione
	while !stop()
		$ i $ $ \leftarrow $ bestMerge($ T $, $ f $, $ Q $)
		$ T $ $ \leftarrow $ merge($ T_{i} $) // Unisce i due intervalli separati da $ t_{i} $
	return T
\end{lstlisting}

L'approccio di questo algoritmo è facilmente intuibile: nella fase di inizializzazione il dataset viene diviso in tanti intervalli quanti sono i distinti valori della feature che si vuole discretizzare, pertanto ogni intervallo conterrà tutte le istanze che hanno lo stesso valore. Nel caso estremo in cui ogni istanza ha un diverso valore per detta feature, allora si inizializzerà un intervallo per ciascuna istanza. Un'ottimizzazione facoltativa che si può introdurre in questa fase è l'unione di tutti quegli intervalli contigui e puri, ovvero contenenti esempi etichettati tutti con la stessa classe, si tratta di fusioni ovvie che si possono fare subito.

\subsection{Normalizzazione e calibrazione}
\index{feature!trasformazione|seealso{normalizzazione}}
\index{normalizzazione}
\index{feature!trasformazione|seealso{calibrazione}}
\index{calibrazione}

La standardizzazione viene spesso utilizzata per fare fronte alla presenza di feature quantitative con scale diverse fra loro; è particolarmente utile in caso di variabili con distribuzione normale, questa trasformazione permette di ottenere i cosiddetti \emph{punti zeta} (detti anche \emph{Z-score}):
\begin{equation}
\textit{z} = \dfrac{\textit{x} - \mu}{\sigma} \nonumber
\end{equation}

\`{E} possibile inoltre andare a trasformare il range delle feature, questa operazione viene detta normalizzazione, andando a "comprimere" il range dei valori nell'intervallo $ [0, 1] $. Quindi dato un valore $ \textit{f} $ per una determinata feature della quale sono noti  il minimo $ \textit{min} $ e il massimo $ \textit{max} $ si può definire il seguente ridimensionamento lineare:
\begin{equation}
\textit{f} \mapsto \dfrac{\textit{f} - \textit{min}}{\textit{max} - \textit{min}} \nonumber
\end{equation}

\section{Selezione delle feature}
\index{feature!selezione}

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
