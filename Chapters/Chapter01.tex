%************************************************
 \chapter{Introduzione}\label{ch:introduction}
%************************************************

L'apprendimento automatico\index{apprendimento!automatico}, da qui in poi \textit{machine learning}\index{machine learning|see {apprendimento automatico}} perché in inglese fa figo, è una disciplina sotto il cappello dell'intelligenza artificiale che studia tecniche e approcci differenti per insegnare ad una macchina l'abilità di apprendere in modo autonomo, migliorare le proprie performance o la propria conoscenza tramite l'esperienza. Tutto ciò senza essere esplicitamente programmata e senza una diretta supervisione dell'uomo.

Il machine learning è la combinazione dei seguenti elementi:
\begin{description}
	\item[task] La descrizione degli obbiettivi, ovvero quali problemi si intende risolvere (classificazione, regressione, clustering, etc...);
	\item[model] La rappresentazione matematica delle soluzioni ai problemi sopra citati (classificatori lineari, alberi decisionali, etc...);
	\item[feature] La rappresentazione degli oggetti coinvolti nei modelli (numeri, categorie, costruzione/selezione di feature, etc...).
\end{description}

In seguito si affronterà nel dettaglio ognuno di questi tre aspetti.

\section{Task}

I task\index{task} possono essere classificati in base agli obiettivi del modello preso in considerazione, in questo modo possono essere divisi in:

\begin{description}
	\item [Task predittivi] Il loro obbiettivo è quello di prevedere una certa variabile (ad esempio una categoria di appartenenza, una valore numerico, un cluster, etc...);
	\item [Task descrittivi] Il loro obbiettivo è quello di individuare una struttura sottostante ai dati.
\end{description}

I task possono anche essere suddivisi in base alla tipologia di apprendimento\index{apprendimento} adottata, nello specifico:

\begin{description}
	\item[Apprendimento supervisionato]\index{apprendimento!supervisionato} Altrimenti noto come \textit{supervised learning}, l'apprendimento viene guidato da esempi corredati dalla loro soluzione (ad esempio per i task di classificazione di immagini apprendono partendo da una serie di immagini alle quali sono già associate le etichette corrette);
	\item[Apprendimento non supervisionato]\index{apprendimento!non supervisionato} Altrimenti noto come \textit{unsupervised learning}, l'apprendimento viene guidato da esempi privi di ulteriori informazioni (ad esempio nelle \ac{SOM} le immagini di esempio sono prive di informazioni sul loro contenuto).
\end{description}

Si possono riassumere nella Tabella \ref{tab:tasks} i diversi tipi di task mettendo insieme i due criteri appena definiti.

\begin{table}
	\centering
	\begin{tabular}{>{\centering\arraybackslash}p{2.3cm}|>{\centering\arraybackslash}p{3.6cm}>{\centering\arraybackslash}p{4.5cm}}
		& Predictive model & Descriptive model \\
		\hline 
		Supervised learning & Classification, regression & Subgroup discovery \\
		Unsupervised learning & Predictive clustering & Descriptive clustering, association rule discovery \\
	\end{tabular}
	\caption{Classificazione dei task basata sugli obbiettivi del modello e sulle modalità di apprendimento.}
	\label{tab:tasks}
\end{table}

\subsection{Task predittivi}\index{task!predittivo}

Come accennato in precedenza lo scopo dei task predittivi è quello di fare inferenza, sperabilmente corretta!, su dati non ancora osservati grazie ad un modello predittivo. Tale modello viene ottenuto in seguito ad una fase di apprendimento su un insieme di esempi chiamato \textit{training set} e deve essere in grado di generalizzare su dati sconosciuti, che non appartengono al suddetto insieme, raccolti in un \textit{test set} indipendente.

Alcuni dei principali esempi di questa categoria di task vengono riportati di seguito:

\begin{description}
	\item[Classificazione binaria e multiclasse]\index{classificazione} Lo scopo è classificare i dati in input in due (binary classification) o più (multiclass classification) categorie;
	\item[Regressione]\index{regressione} L'associazione non viene fatta con un valore appartenente ad un insieme finito, bensì con un valore reale risultante da una funzione (il modello) in grado di rappresentare i punti nello spazio;
	\item[Clustering predittivo]\index{clustering} L'obbiettivo è determinare il cluster di appartenenza di una determinata istanza basandosi su un criterio di vicinanza.
\end{description}

Gli esempi elencati sono i più diffusi nell'ambito dell'apprendimento automatico, ma non gli unici. Si possono ancora menzionare i task di stima della probabilità, scoring e ranking, i quali verranno affrontati successivamente.

\subsection{Il problema dell'overfitting}\label{ssec:overfitting}
\index{overfitting}
\index{sovradattamento|see{overfitting}}

Il principale problema che affligge in particolare i task predittivi nell'ambito del machine learning è l'\textit{overfitting} (letteralmente sovradattamento, eccessivo adattamento). In termini pratici questo si manifesta in un modello estremamente performante sul training set, ma inefficiente sul test set, o più in generale su qualsiasi nuova istanza al di fuori di quelle usate in fase di addestramento.

Quanto appena descritto si rivela particolarmente problematico: un modello che soffre di questo problema si rivela inutile perché inutilizzabile per fare inferenza corretta su dati che non appartengono al training set, scopo per il quale invece sarebbe destinato.

Proprio per valutare le capacità del modello di generalizzare correttamente si è soliti dividere il dataset in due insiemi diversi di istanze:
\begin{description}
	\item[Training set]\index{dataset!training set} L'insieme delle istanze utilizzate per la fase di addestramento;
	\item[Test set]\index{dataset!test set} Un insieme di istanze diverse da quelle contenute nel training set utilizzate per la verifica delle prestazioni e la capacità del modello di generalizzare adeguatamente.
\end{description}

Nella valutazione delle performance quindi non si deve puntare solamente a massimizzare quelle sul training set, si rischierebbe di cadere nella trappola dell'overfitting, bensì si deve cercare di minimizzare la distanza fra le performance sul training set e le performance sul test set. Questo vale a dire che il modello è in grado di generalizzare bene o molto bene, riuscendo a classificare efficacemente anche tutte le istanze nuove, mai viste durante l'addestramento. Al contrario quando le performance sul test set si discostano maggiormente si ha un modello eccessivamente adattato al training set.

\subsection{Task descrittivi}\label{ssec:descr_tasks}\index{task!descrittivo}

L'obbiettivo di questo tipo di task è esplicitare una struttura sottostante ai dati, ad esempio i legami funzionali fra gli attributi delle istanze, la distribuzione delle istanze in alcune classi, notevoli generalizzazioni e astrazioni del campione di dati. Verranno trattati in modo esaustivo nella sottosezione \ref{ssec:sub_discovery}.

Fra gli esempi principali di questi task ci sono quelli già citati nella Tabella \ref{tab:tasks}:
\begin{description}
	\item[Subgroup discovery] L'obbiettivo è la ricerca di un sottoinsieme di istanze nel dataset con caratteristiche peculiari e comuni, come ad esempio una particolare distribuzione delle classi rispetto all'insieme di partenza;
	\item[Descriptive clustering] L'obbiettivo è la ricerca dei possibili cluster con i quali si potrebbero classificare tutte le istanze nel dataset;
	\item[Association rule discovery] L'obbiettivo è la ricerca di regole ricorrenti in diversi attributi delle istanze nel dataset (si veda la sottosezione \ref{ssec:association_rules}).
\end{description}

Si noti che il clustering può quindi essere declinato in due versioni differenti: nei task predittivi i cluster sono già noti e si devono classificare al loro interno le nuove istanze, invece nei task descrittivi dalle istanze del training set si devono scoprire i cluster in cui dividerle.

\section{Model}\index{modello}

Dopo aver discusso approfonditamente i diversi tipi di problemi che il machine learning può affrontare, si deve dare spazio ad un aspetto ugualmente importante: i modelli, ovvero \textit{come} nella fase di apprendimento vengono mappati i dati in input descritti sotto forma di feature agli output opportuni. I modelli si possono classificare essenzialmente in tre categorie sicuramente rappresentative ma non esaustive:

\begin{description}
	\item[Modelli geometrici] I dati vengono mappati in uno spazio geometrico e la soluzione è descrivibile anch'essa geometricamente, ad esempio una retta che divide un piano;
	\item[Modelli probabilistici] Il dominio viene descritto attraverso elementi probabilistici, associando ai dati una probabilità, la quale permette di fare inferenza probabilistica o calcolare distribuzioni probabilistiche per ridurre l'incertezza\footnote{Ci scusiamo per l'abuso del termine \textit{probabilità} e dei suoi derivati, probabilmente abbiamo esagerato.};
	\item[Modelli logici] Sono costruiti con formule logiche grazie alle quali è possibile fare inferenza.
\end{description}

Si prenda ora in considerazione uno dei più famosi esempi nell'ambito del machine learning, il filtro antispam SpamAssassin. Con questo esempio verranno presentati i diversi tipi di modelli che si possono realizzare per questo problema di classificazione binaria.

\subsection{Modelli geometrici}\index{modello!geometrico}

Lo scopo del filtro per le e-mail è identificare nella posta in arrivo quali messaggi sono da considerarsi spam (\textit{junk mail}). SpamAssassin è un filtro antispam molto diffuso che, alla ricezione di una e-mail, analizza testo, immagini, mail server, collegamenti, etc... con una serie di test booleani assegnando loro dei pesi che possono essere positivi, negativi o nulli. Se la somma finale di tali pesi supera una determinata soglia (in questo specifico filtro corrisponde a $ 5 $) allora la e-mail viene classificata come spam.

Si faccia ora riferimento alla tabella \ref{tab:spamassassin_example} nella quale sono riportati degli esempi numerici. Nella prima colonna sono elencate quattro e-mail; $ x_{1} $ e $ x_{2} $ sono le due feature, ovvero i due test condotti su ciascuna e-mail, se il risultato del test è positivo il valore assegnato alla feature è pari a $ 1 $, $ 0 $ altrimenti; la colonna Spam? contiene un'etichetta fornita dall'utente ($ 1 $ spam, $ 0 $ altrimenti) che classifica le mail per addestrare il sistema; l'ultima colonna contiene il possibile modello proposto, descritto dalla formula $ 4x_{1}+4x_{2} $, con il quale si potranno valutare le nuove e-mail in arrivo. L'introduzione di una soglia permette infine di poter classificare le e-mail: se il modello fornisce un valore in output strettamente maggiore di $ 5 $ allora si tratterà di spam, come per la prima istanza delle quattro proposte nell'esempio. Si noti come il modello classifichi correttamente le e-mail rispetto alle etichette fornite dall'utente. \`{E} altresì chiaro che il modello proposto in questo esempio non è l'unico possibile, ve ne possono infatti essere diversi, potenzialmente infiniti, in grado di fornire un output che ci permetta di svolgere lo stesso task di classificazione in maniera ugualmente corretta.

\begin{table}
	\centering
	\begin{tabular}{ccccc}
		\toprule
		E-mail & $ x_{1} $ & $ x_{2} $ & Spam? & $ 4x_{1}+4x_{2} $ \\
		\midrule
		$ 1 $ & $ 1 $ & $ 1 $ & $ 1 $ & $ 8 $ \\
		$ 2 $ & $ 0 $ & $ 0 $ & $ 0 $ & $ 0 $ \\
		$ 3 $ & $ 1 $ & $ 0 $ & $ 0 $ & $ 4 $ \\
		$ 4 $ & $ 0 $ & $ 1 $ & $ 0 $ & $ 4 $ \\
		\bottomrule
	\end{tabular}
	\caption{Tabella con valori numerici di esempio per diverse combinazioni di risultati dei test di verifica sulle e-mail.}
	\label{tab:spamassassin_example}
\end{table}\medskip

Riassumendo si possono identificare i tre elementi introdotti in precedenza:
\begin{description}
	\item[task] La classificazione delle e-mail in entrata;
	\item[model] La funzione definita per calcolare il punteggio di ogni e-mail da valutare, quindi un modello lineare;
	\item[features] I risultati dei due test, $ x_{1} $ e $ x_{2} $.
\end{description}

\subsubsection{Classificatori lineari}\index{classificatore!lineare|seealso{modello lineare}}

Come già accennato l'esempio appena affrontato poc'anzi si può risolvere con un classificatore lineare. Per determinare tale modello si devono trovare dei pesi tali per cui si ottiene una soglia che divide esattamente (o come meglio è possibile) gli esempi nelle due classi.

\begin{figure}
	\centering
	\includegraphics[width=0.8\textwidth]{Images/ClassificatoreLineare}
	\caption{Un esempio grafico di classificatore lineare.}
	\label{fig:classificatore_lineare}
\end{figure}

Tale concetto è espresso graficamente e molto intuitivamente dalla figura \ref{fig:classificatore_lineare} nella quale si può identificare il vettore dei pesi $ \textbf{w} $ evidenziato in rosso, perpendicolare alla soglia che divide gli esempi altrimenti nota come \textit{decision boundary} o \textit{linear boundary}.

Quanto espresso a parole e graficamente può essere formalizzato come segue:
\begin{itemize}
	\item $ \textit{x}_{\textit{i}} = \{0, 1\} $, risultato binario dell'i-esimo test;
	\item $ \textbf{w} = \{\textit{w}_{1}, \textit{w}_{2}, ..., \textit{w}_{\textit{n}}\} $, il vettore degli $ \textit{n} $ pesi;
	\item $ \sum_{\textit{i}=1}^{\textit{n}}\textit{w}_\textit{i}\cdot \textit{x}_{\textit{i}} > t $, modello lineare tramite il quale si classifica un input con una soglia di decisione $ t $.
\end{itemize}

Nella figura \ref{fig:aaut_schema} si possono identificare le diverse parti che compongono il processo di apprendimento automatico e come sono fra loro relazionate per i task di tipo predittivo.

\begin{figure}
	\centering
	\includegraphics[width=1\textwidth]{Images/MachineLearning_Overview}
	\caption{Schema riassuntivo del funzionamento del machine learning per task di tipo predittivo.}
	\label{fig:aaut_schema}
\end{figure}

Il task si definisce come l'insieme di dati che possono essere osservati e modellati al fine di ottenere l'output desiderato.

Il modello per raggiungere il suddetto task è solitamente parametrizzato (ad esempio con dei pesi, come nell'esempio di SpamAssassin). Tramite i parametri si può migliorare e raffinare il modello affinché sia più preciso e acquisisca maggiore esperienza. Il learning problem è realizzare un algoritmo di apprendimento, il learning algorithm, capace di trovare i giusti parametri per il modello.

I dati coinvolti nel processo di addestramento possono essere grezzi e inadeguati, pertanto è opportuno definire adeguatamente delle feature che diano un input ad hoc per la realizzazione del modello.

Applicando quanto schematizzato in figura \ref{fig:aaut_schema} all'esempio del filtro antispam si ottiene lo schema in figura \ref{fig:spamassassin_geom} dove si possono facilmente identificare tutte le parti viste in precedenza, in particolare il modello geometrico, ovvero il classificatore lineare, che si vuole ottenere.

\begin{figure}
	\centering
	\includegraphics[width=1\textwidth]{Images/SpamAssassin_Geom}
	\caption{Schema del funzionamento del machine learning nello specifico esempio del filtro antispam SpamAssassin per ottenere un modello geometrico.}
	\label{fig:spamassassin_geom}
\end{figure}

Il learning problem consiste nell'apprendere un modello per classificare correttamente le e-mail in input. A questo fine si adotta un classificatore lineare, ovvero un modello geometrico, i cui pesi vengono definiti dall'algoritmo di apprendimento grazie al training set, ovvero le e-mail inizialmente etichettate dall'utente. I pesi ottenuti dalla fase di addestramento sono un sottoinsieme delle possibili combinazioni accettabili per questo specifico task. Le features sono i test condotti dal filtro antispam che forniscono un input adeguato al classificatore.

\subsection{Modelli probabilistici}\index{modello!probabilistico}

Come anticipato in precedenza questa tipologia di modelli si basa sulla teoria della probabilità attraverso la quale vengono descritti i dati, le variabili e i modelli.

Date due variabili aleatorie:
\begin{itemize}
	\item X, i dati in input, possono essere un unico valore o un vettore di valori;
	\item Y, i dati in output.
\end{itemize}

Ciò che si desidera scoprire è la \textit{maximum a posteriori probability}\index{Maximum A Posteriori}, che successivamente viene abbreviata con $ Y_{MAP} $, ovvero il valore di $ Y $ che massimizza la probabilità a posteriori $ \textit{P}(Y|X) $.
\begin{eqnarray}
Y_{MAP} &=& \arg \max_{Y}\ \textit{P}(Y|X) \nonumber \\
		&=& \arg \max_{Y}\ \frac{\textit{P}(X|Y) \cdot \textit{P}(Y)}{\textit{P}(X)} \nonumber \\
		&=& \arg \max_{Y}\ \textit{P}(X|Y) \cdot \textit{P}(Y) \label{eqn:Ymap_calculation}
\end{eqnarray}

Nel primo passaggio viene applicata la regola di Bayes per il calcolo delle probabilità, per cui si sa valere la seguente uguaglianza:
\begin{equation}
\textit{P}(Y|X) = \frac{\textit{P}(X|Y) \cdot \textit{P}(Y)}{\textit{P}(X)} \nonumber
\end{equation}

Nel secondo passaggio invece si elimina il denominatore. Poiché si intende trovare il massimo valore variando la variabile aleatoria $ Y $ e il fattore $ \textit{P}(X) $ è indipendente rispetto ad essa, allora lo si può ignorare e considerare solo il numeratore.

Oltre a $ Y_{MAP} $ è di interesse il calcolo di $ Y_{ML} $, ovvero il valore di \textit{maximum likelihood}\index{Maximum Likelihood}, la funzione di verosimiglianza. A volte infatti si può ignorare la distribuzione di $ \textit{P}(Y) $ (o assumere che sia uniforme), per questo $ Y_{MAP} $ si può ridurre a:
\begin{equation}
Y_{ML} = \arg \max_{Y}\ \textit{P}(X|Y) \nonumber
\end{equation}

Nonostante $ Y_{ML} $ sia una approssimazione, all'atto pratico e sotto le dovute condizioni spesso risulta funzionare bene.

\subsubsection{Modello probabilistico per SpamAssassin}

Riprendendo l'esempio del filtro per le e-mail contenenti spam si ha:
\begin{itemize}
	\item $ \textbf{X} = \{X_{1} = Viagra, X_{2} = Lottery\} $, l'input è un vettore di valori binari che indicano se determinate parole sono presenti o meno nel messaggio, ad esempio $ \textbf{X} = \{X_{1} = 0, X_{2} = 1\} $ indica che non è presente la parola $ Viagra $ associata alla variabile aleatoria $ X_{1} $, e che invece è presente la parola $ Lottery $ associata ad $ X_{2} $;
	\item $ \textbf{Y} = \{Ham,\ Spam\} $, l'output è una classificazione delle e-mail fra i due possibili valori $ Ham $ o $ Spam $;
	\item Si assume che  un messaggio e-mail sia spam nel $ 30\% $ dei casi, quindi si ha $ \textit{P}(spam) = 0.3 $;
	\item nella tabella \ref{tab:spamassassin_joint_distribution} è riassunta la distribuzione congiunta per tutti i calcoli probabilistici che seguiranno;
	\item nella tabella \ref{tab:spamassassin_prob} sono riportate le stime delle probabilità che le e-mail siano spam riferite all'osservazione dei termini Viagra e Lottery nei messaggi già ricevuti.
\end{itemize}

\begin{table}
	\centering
	\begin{tabular}{cccc}
		\toprule
		Viagra & Lottery & Spam & Probabilità \\
		\midrule
		$ 0 $ & $ 0 $ & $ 0 $ & $ 0.6 $ \\
		$ 0 $ & $ 0 $ & $ 1 $ & $ 0.01 $ \\
		$ 0 $ & $ 1 $ & $ 0 $ & $ 0.06 $ \\
		$ 0 $ & $ 1 $ & $ 1 $ & $ 0.02 $ \\
		$ 1 $ & $ 0 $ & $ 0 $ & $ 0.03 $ \\
		$ 1 $ & $ 0 $ & $ 1 $ & $ 0.07 $ \\
		$ 1 $ & $ 1 $ & $ 0 $ & $ 0.01 $ \\
		$ 1 $ & $ 1 $ & $ 1 $ & $ 0.2 $ \\
		\bottomrule
	\end{tabular}
	\caption{Tabella riassuntiva della distribuzione congiunta per l'esempio del filtro SpamAssassin. Si noti che la somma delle probabilità corrispondenti alla classe $ spam $ sia proprio $ 0.3 $.}
	\label{tab:spamassassin_joint_distribution}
\end{table}

\begin{table}
	\centering
	\begin{tabular}{ccccc}
		\toprule
		$ X_{1} $ & $ X_{2} $ & $ \textit{P}(X_{1}X_{2}|spam) $ & $ \textit{P}(X_{1}X_{2}|\neg spam) $ & $ \textit{P}(X_{1}, X_{2}) $ \\
		\midrule
		$ 0 $ & $ 0 $ & $ 0.033 $ & $ 0.857 $ & $ 0.61 $ \\
		$ 0 $ & $ 1 $ & $ 0.067 $ & $ 0.086 $ & $ 0.08 $ \\
		$ 1 $ & $ 0 $ & $ 0.233 $ & $ 0.043 $ & $ 0.1 $ \\
		$ 1 $ & $ 1 $ & $ 0.667 $ & $ 0.014 $ & $ 0.21 $ \\
		\bottomrule
	\end{tabular}
	\caption{Tabella riassuntiva delle probabilità stimate dall'osservazione di tutti i messaggi e-mail ricevuti in precedenza associate alla presenza o meno delle parole riferite dalle due variabili aleatorie nel vettore in input $ X = \{X_{1} = Viagra, X_{2} = Lottery\} $.}
	\label{tab:spamassassin_prob}
\end{table}

\subsubsection{Calcolo della maximum a posteriori probability}

Il modello probabilistico definito per l'esempio del filtro SpamAssassin può essere utilizzato in diversi modi per classificare le e-mail in arrivo. Nel primo esempio le e-mail vengono classificate stimando la probabilità $ Y_{MAP} $ che siano spam attraverso la formula $ \textit{P}(spam|X_{1}X_{2}) $ già introdotta con la precedente formula \ref{eqn:Ymap_calculation} nella sua versione originale. Se il risultato è maggiore o uguale alla soglia fissata a $ 0.5 $ allora il messaggio viene etichettato come spam. Nella tabella \ref{tab:spamassassin_ymap1} vengono riportati i valori calcolati per la \textit{maximum a posteriori probability} e la conseguente classificazione.

\begin{table}
	\centering
	\begin{tabular}{cccc}
		\toprule
		$ X_{1} $ & $ X_{2} $ & $ \textit{P}(spam|X_{1}X_{2}) $ & $ Y_{MAP} $ \\
		\midrule
		$ 0 $ & $ 0 $ & $ 0.164 $ & not spam \\
		$ 0 $ & $ 1 $ & $ 0.25 $ & not spam \\
		$ 1 $ & $ 0 $ & $ 0.7 $ & spam \\
		$ 1 $ & $ 1 $ & $ 0.95 $ & spam \\
		\toprule
	\end{tabular}
	\caption{Calcolo di $ Y_{MAP} $ e classificazione con la formula $ \textit{P}(spam|X_{1}X_{2}) \geq 0.5 $.}
	\label{tab:spamassassin_ymap1}
\end{table}

Un secondo modo per ottenere lo stesso risultato in termini di classificazione dei messaggi in arrivo è utilizzare la formula \ref{eqn:Ymap_calculation} per la stima della probabilità. I calcoli relativi a questo approccio sono riportati nella tabella \ref{tab:spamassassin_ymap2}, in evidenza sono stati posti i valori massimi selezionati dalla funzione $ \arg \max_{Y \in \{spam, \neg spam\}} $ che hanno determinato la classificazione.

\begin{table}
	\centering
	\begin{tabular}{ccccc}
		\toprule
		$ X_{1} $ & $ X_{2} $ & $ \textit{P}(X_{1}X_{2}|spam)\textit{P}(spam) $ & $ \textit{P}(X_{1}X_{2}|\neg spam)\textit{P}(\neg spam) $ & $ Y_{MAP} $ \\
		\midrule
		$ 0 $ & $ 0 $ & $ 0.033\cdot 0.3=0.0099 $ & $ 0.857\cdot 0.7=\textbf{0.5999} $ & not spam \\
		$ 0 $ & $ 1 $ & $ 0.067\cdot 0.3=0.0201 $ & $ 0.086\cdot 0.7=\textbf{0.0602} $ & not spam \\
		$ 1 $ & $ 0 $ & $ 0.233\cdot 0.3=\textbf{0.0699} $ & $ 0.043\cdot 0.7=0.0301 $ & spam \\
		$ 1 $ & $ 1 $ & $ 0.667\cdot 0.3=\textbf{0.2001} $ & $ 0.014\cdot 0.7=0.0098 $ & spam \\
		\toprule
	\end{tabular}
	\caption{Calcolo di $ Y_{MAP} $ e classificazione con la formula $ \arg \max_{Y \in \{spam, \neg spam\}}\ \textit{P}(X_{1}X_{2}|Y)\textit{P}(Y) $.}
	\label{tab:spamassassin_ymap2}
\end{table}

Nonostante i valori numericamente diversi i due approcci hanno prodotto risultati in termini di classificazione ugualmente corretti, d'altra parte tale conclusione è quasi ovvia dato che i due approcci utilizzano la stessa formula, sebbene in due formulazioni diverse.

\subsubsection{Calcolo della \textit{maximum likelihood probability}}

Dopo aver verificato l'efficacia della \textit{maximum a posteriori probability} nelle sue due diverse formulazioni, è di interesse mettere alla prova anche la funzione di verosimiglianza per vedere come si comporta con l'esempio del filtro SpamAssassin.

Nella tabella \ref{tab:spamassassin_yml} sono riportati i valori di $ Y_{ML} $ per le diverse combinazioni di parole presenti nelle e-mail. Si nota subito la semplificazione rispetto ai calcoli che invece si erano resi necessari nella tabella \ref{tab:spamassassin_ymap2} dove si teneva conto anche di $ P(Y) $.

\begin{table}
	\centering
	\begin{tabular}{ccccc}
		\hline
		$ X_{1} $ & $ X_{2} $ & $ P(X_{1}X_{2}|spam) $ & $ P(X_{1}X_{2}|\neg spam) $ & $ Y_{ML} $ \\
		\hline
		$ 0 $ & $ 0 $ & $ 0.033 $ & $ \textbf{0.857} $ & not spam \\
		$ 0 $ & $ 1 $ & $ 0.067 $ & $ \textbf{0.086} $ & not spam \\
		$ 1 $ & $ 0 $ & $ \textbf{0.233} $ & $ 0.043 $ & spam \\
		$ 1 $ & $ 1 $ & $ \textbf{0.667} $ & $ 0.014 $ & spam \\
		\hline
	\end{tabular}
	\caption{Calcolo della probabilità $ Y_{ML} $ e classificazione con la formula $ \arg \max_{Y \in \{spam, \neg spam\}}\ \textit{P}(X_{1}X_{2}|Y) $.}
	\label{tab:spamassassin_yml}
\end{table}

\`{E} facile vedere come il risultato approssimato con $ Y_{ML} $ sia chiaramente corretto come nei primi due casi dove si calcolava la soluzione esatta con $ Y_{MAP} $.

\subsubsection{Assunzione \textit{naive} di Bayes}\index{Bayes!assunzione naive}

Ci si può rendere conto come con un numero più alto di variabili aleatorie, ovvero di feature in input, le tabelle di cui sopra diventerebbero enormemente lunghe, infatti le loro righe crescono esponenzialmente rispetto al numero di variabili aleatorie in input. Inoltre per ogni possibile diversa combinazione delle feature bisognerebbe disporre di diversi esempi per poter avere delle accurate stime probabilistiche, un dataset di esempi enorme perché le suddette combinazioni sono anch'esse esponenziali.

Per ovviare a questo gravoso problema computazionale si può fare una assunzione \textit{naive}, grezza, la più semplice che si possa fare. Si assume che le variabili $ X $ in input siano indipendenti rispetto ad $ Y $. In questo modo si riduce il problema a tabelle che crescono linearmente invece che esponenzialmente: molto meglio!

Questa assunzione in realtà viene violata molto spesso, le variabili aleatorie in input possono essere correlate fra loro e non sempre essere tutte indipendenti. Nonostante ciò il risultato dell'applicazione di questa semplificazione si rivela spesso molto efficace, anche se i risultati non sono perfetti.

La conseguenza di questa assunzione \textit{naive} è che a questo punto basta valutare la probabilità correlate alle singole variabili aleatorie in input. Per l'esempio del filtro SpamAssassin si ottengono le tabelle \ref{tab:spamassassin_reduced1} e \ref{tab:spamassassin_reduced2}, decisamente più semplici rispetto alla tabella \ref{tab:spamassassin_prob}.

\begin{table}
	\centering
	\begin{tabular}{ccc}
		\toprule
		$ X_{1} $ & $ \textit{P}(X_{1}|spam) $ & $ \textit{P}(X_{1}|\neg spam) $ \\
		\midrule
		$ 0 $ & $ 0.1 $ & $ 0.943 $ \\
		$ 1 $ & $ 0.9 $ & $ 0.057 $ \\
		\bottomrule
	\end{tabular}
	\caption{Probabilità che una e-mail contenga o meno spam considerando che la variabile aleatoria $ X_{1} $ sia indipendente da $ X_{2} $.}
	\label{tab:spamassassin_reduced1}
\end{table}

\begin{table}
	\centering
	\begin{tabular}{ccc}
		\toprule
		$ X_{2} $ & $ \textit{P}(X_{2}|spam) $ & $ \textit{P}(X_{2}|\neg spam) $ \\
		\midrule
		$ 0 $ & $ 0.27 $ & $ 0.9 $ \\
		$ 1 $ & $ 0.73 $ & $ 0.1 $ \\
		\bottomrule
	\end{tabular}
	\caption{Probabilità che una e-mail contenga o meno spam considerando che la variabile aleatoria $ X_{2} $ sia indipendente da $ X_{1} $.}
	\label{tab:spamassassin_reduced2}
\end{table}

Questa semplificazione ovviamente produce a cascata degli effetti sul passaggio successivo, il calcolo della probabilità a posteriori $ Y_{MAP} $ che viene rifatto e riportato nella tabella \ref{tab:spamassassin_ymap_simple}.

La formula per il calcolo del valore di $ Y_{MAP} $ per questo esempio è:
\begin{eqnarray}
Y_{MAP} &=& \arg \max_{Y \in \{spam, \neg spam\}} \textit{P}(X_{1}X_{2}|Y)\textit{P}(Y) \nonumber \\
&=& \arg \max_{Y \in \{spam, \neg spam\}} \textit{P}(X_{1}|Y)\textit{P}(X_{2}|Y)\textit{P}(Y) \nonumber
\end{eqnarray}

\begin{table}
\centering
\begin{tabular}{>{\centering\arraybackslash}p{0.6cm}>{\centering\arraybackslash}p{0.6cm}>{\centering\arraybackslash}p{3.3cm}>{\centering\arraybackslash}p{3.3cm}>{\centering\arraybackslash}p{1.75cm}}
	\hline
	\vspace{1.2pt}$ X_{1} $ & \vspace{1.2pt}$ X_{2} $ & $ \textit{P}(X_{1}|spam)\cdot \newline \textit{P}(X_{2}|spam)\cdot \newline \textit{P}(spam) $ & $ \textit{P}(X_{1}|\neg spam)\cdot \newline \textit{P}(X_{2}|\neg spam)\cdot \newline \textit{P}(\neg spam) $ & \vspace{1.2pt}$ Y_{MAP} $ \\
	\hline
	$ 0 $ & $ 0 $ & $ 0.0081 $ & $ \textbf{0.594} $ & not spam \\
	$ 0 $ & $ 1 $ & $ 0.0219 $ & $ \textbf{0.066} $ & not spam \\
	$ 1 $ & $ 0 $ & $ \textbf{0.0729} $ & $ 0.036 $ & spam \\
	$ 1 $ & $ 1 $ & $ \textbf{0.1971} $ & $ 0.004 $ & spam \\
	\hline
\end{tabular}
\caption{Calcolo della probabilità $ Y_{ML} $ e classificazione con la formula $ \arg \max_{Y\in\{spam, \neg spam\}}\ \textit{P}(X_{1}X_{2}|Y) $.}
\label{tab:spamassassin_ymap_simple}
\end{table}

Applicando quanto schematizzato in figura \ref{fig:aaut_schema} all'esempio del filtro antispam si ottiene lo schema in figura \ref{fig:spamassassin_prob} dove si possono facilmente identificare tutte le parti viste in precedenza, in particolare il modello probabilistico che si vuole ottenere con le relative probabilità stimate.

\begin{figure}
	\centering
	\includegraphics[width=1\textwidth]{Images/SpamAssassin_Prob}
	\caption{Schema del funzionamento del machine learning nello specifico esempio del filtro antispam SpamAssassin per ottenere un modello probabilistico.}
	\label{fig:spamassassin_prob}
\end{figure}

\subsection{Modelli logici}

Per il task di esempio utilizzato, il filtro antispam SpamAssassin, sono stati proposti un modello geometrico, ovvero un classificatore lineare, ed un modello probabilistico. Di seguito si proporrà un ultimo modello, questa volta di tipo logico, al fine di dimostrare come sovente sia possibile approcciarsi in molteplici modi alla risoluzione di uno stesso problema.

Un classificatore logico cerca di costruire delle regole con le quali poter svolgere inferenza. Nell'esempio di SpamAssassin alcune regole potrebbero essere espresse informalmente come segue:
\begin{itemize}
	\item "Se la e-mail contiene la parola \textit{Viagra} allora la stima della probabilità che si tratti di spam è 4:1\footnote{4:1 indica che è 4 volte più probabile che la e-mail sia spam piuttosto che non lo sia.}";
	\item "Se la e-mail contiene la frase \textit{blue pill} allora la stima della probabilità che si tratti di spam è 3:1";
	\item "In tutti gli altri casi la stima della probabilità che si tratti di spam è 1:6\footnote{1:6 indica che è 6 volte più probabile che la e-mail non sia spam piuttosto che lo sia.}".
\end{itemize}

La prima regola copre tutte le e-mail che contengono la parola "Viagra", indipendentemente dal fatto che contengano anche la coppia di parole "blue pill"; la seconda regola riguarda tutte le e-mail contenenti esclusivamente la coppia di parole "blue pill", così da non sovrapporsi con la precedente; la terza regola copre tutte le e-mail rimanenti. Come queste regole sono state costruite viene affrontato nei capitoli successivi, ad esempio utilizzando gli alberi di decisione.

Applicando quanto schematizzato in figura \ref{fig:aaut_schema} all'esempio del filtro antispam si ottiene lo schema in figura \ref{fig:spamassassin_logic} dove si possono facilmente identificare tutte le parti viste in precedenza, in particolare il modello logico che si vuole ottenere con le relative regole decisionali che permettono di fare inferenza.

\begin{figure}
	\centering
	\includegraphics[width=1\textwidth]{Images/SpamAssassin_Logic}
	\caption{Schema del funzionamento del machine learning nello specifico esempio del filtro antispam SpamAssassin per ottenere un modello logico.}
	\label{fig:spamassassin_logic}
\end{figure}

\section{Feature}\index{feature}

La scelta e la manipolazione delle feature in relazione al task si rivela un passaggio molto importante nelle tecniche di machine learning, è un fattore decisivo che permette al modello di essere efficace nell'apprendere ed essere performante. In particolare bisogna capire quali feature vanno tenute in considerazione in base al problema che si vuole risolvere, soprattutto perché non sempre tutti i dati di cui si dispone sono utili per ogni task, anche solo una parte può essere sufficiente, una parte che non necessariamente deve comprendere i dati che apparentemente possono essere considerati importanti e che in realtà sono irrilevanti per alcuni problemi.

Con l'ausilio di tre diversi esempi si approfondirà come la scelta delle feature e la loro manipolazione si possa rivelare assai rilevante nei processi di apprendimento automatico.

\subsection{Scelta delle feature}\label{ssec:intro_feature}

Il primo esempio che viene introdotto per spiegare come la scelta delle feature si riveli fondamentale per la risoluzione di problemi di apprendimento automatico è l'approssimazione di una funzione in un determinato intervallo.

\begin{figure}
	\centering
	\includegraphics[width=0.8\textwidth]{Images/FunctionApprox}
	\caption{Esempio di approssimazione della funzione $ y = \cos \pi x $ (in blu) nell'intervallo $ -1 \le x \le 1 $.}
	\label{fig:example_function_approx}
\end{figure}

\begin{figure}
	\centering
	\includegraphics[width=0.4\textwidth]{Images/RegTree_Ex}
	\caption{Esempio di approssimazione della funzione $ y = \cos \pi x $ nell'intervallo $ -1 \le x \le 1 $ con un albero di regressione che usa la variabile $ \textit{x} $ per lo split e dei classificatori lineari all'interno delle foglie.}
	\label{fig:combo_reg_tree_linear_class}
\end{figure}

La funzione scelta è $ y = \cos \pi x $ nell'intervallo $ -1 \le x \le 1 $, vedasi figura \ref{fig:example_function_approx}. L'approssimazione più grezza e banale è la retta di equazione $ y = 0 $, la quale ovviamente non approssima per niente la curva nell'intervallo desiderato. Tuttavia se si sceglie di spezzare l'intervallo in due intervalli uguali quali $ -1 \le x < 0 $ e $ 0 \le x \le 1 $ si possono trovare facilmente due funzioni lineari che approssimano molto meglio la curva. Per fare ciò quindi la variabile $ \textit{x} $ diventa la scelta migliore sia come feature di split, sia come feature di regressione, nella soluzione che unisce due modelli riportata in figura \ref{fig:combo_reg_tree_linear_class}.

\begin{figure}
	\centering
	\includegraphics[width=1\textwidth]{Images/Diabete1}
	\caption{Grafico sull'incidenza del diabete nelle persone in base al loro peso riportato sull'asse delle ascisse con campionamento ogni 10 chilogrammi.}
	\label{fig:diabete_example1}
\end{figure}

Un secondo esempio che mette in risalto quanto sia importante una scelta adeguata delle feature che si prendono in considerazione nel machine learning è basato su dei dati legati alla malattia del diabete. In particolare nel grafico riportato in figura \ref{fig:diabete_example1} si combina un istogramma le cui barre verticali rappresentano il numero di persone con o senza il diabete, come specificato nella legenda, mentre in verde viene evidenziata la percentuale di persone con il diabete; sull'asse delle ascisse viene specificato il peso delle persone in chilogrammi.

Il task in questo esempio è predire il numero di persone con o senza diabete in base ai dati a disposizione. Un possibile modo per svolgere questa predizione è valutare la percentuale come nel grafico in figura \ref{fig:diabete_example1} ed utilizzare la curva, o meglio la linea spezzata verde che si ottiene. Purtroppo in questo caso detta linea non è per niente regolare, ha alti e bassi, sale e scende, è rumorosa, e di conseguenza non affidabile per fare delle predizioni, al punto che si potrebbe anche mettere in dubbio la correlazione fra peso e incidenza del diabete nelle persone. Tuttavia questa correlazione c'è, ed è possibile farla risaltare dal grafico con una attenta scelta su una feature: in particolare se la linea ottenuta è troppo irregolare, proprio come in questo caso, il motivo può essere dato da un eccessivo campionamento, un livello di dettaglio troppo alto per le necessità del problema da risolvere.

\begin{figure}
	\centering
	\includegraphics[width=1\textwidth]{Images/Diabete2}
	\caption{Grafico sull'incidenza del diabete nelle persone in base al loro peso riportato sull'asse delle ascisse con campionamento ogni 20 chilogrammi.}
	\label{fig:diabete_example2}
\end{figure}

Nella figura \ref{fig:diabete_example1} il campionamento dei dati sul diabete veniva fatto dividendo le persone in fasce di $ 10 $ chilogrammi, ma se si modifica leggermente questa feature raggruppando le persone in fasce di $ 20 $ chilogrammi allora si ottiene il risultato in figura \ref{fig:diabete_example2}, il quale ha quindi una linea verde molto più regolare ed adatta allo scopo di fare predizioni precise ed accurate sull'incidenza della malattia.

Grazie a questo secondo esempio quindi è chiaro che la scelta delle feature è essenziale per permettere agli algoritmi di apprendimento automatico di risolvere in modo intelligente e performante i problemi. Se per il diabete avessimo scelto il numero di dita di una persona al posto del suo peso per stimare con quale probabilità avrebbe potuto essere affetta da diabete, chiaramente sarebbe stato impossibile venire a capo del problema con una stima sensata.

In particolare nella scelta delle feature si deve tenere ulteriormente conto della loro rappresentazione, argomento che verrà ampiamente discusso nel capitolo \ref{ch:features}, che nell'esempio del diabete corrispondeva alla granularità scelta nella suddivisione dei dati.

\subsection{Modifica delle feature}\label{ssec:intro_feature_transf}

Con un ultimo esempio introduciamo la modifica delle feature, ovvero come è possibile trasformarle per adattarle meglio agli scopi dei task di apprendimento automatico. Anche questa parte viene trattata più in dettaglio nel capitolo \ref{ch:features}. L'esempio che viene preso in considerazione è una introduzione all'argomento trattato successivamente nella sezione \ref{sc:kernel}.

La modifica e la trasformazione delle feature è un argomento molto ampio che può essere declinato in molteplici modi. In questo esempio si vedrà come un mapping delle feature in un nuovo spazio, quindi sostanzialmente una loro modifica sistematica, permetta di ricondurre un problema ad un altro di più facile risoluzione.

\begin{figure}
	\centering
	\includegraphics[width=1\textwidth]{Images/Mapping_NewSpace}
	\caption{Esempio di trasformazione delle feature. Nel grafico a sinistra i punti corrispondono agli esempi originali con coordinate $ (x, y) $; nel grafico a destra sono riportati gli stessi esempi rimappati con coordinate modificate $ (x^{2}, y^{2}) $.}
	\label{fig:mapping_new_space}
\end{figure}

Si prenda in considerazione il primo dei due grafici riportati in figura \ref{fig:mapping_new_space}. Come è facile notare i due insieme di punti rossi e blu non sono linearmente separabili, per poter svolgere la classificazione bisognerebbe trovare l'equazione di una funzione di almeno secondo grado. Un problema non banale, tuttavia facilmente risolvibile con una semplice operazione: una modifica delle feature che consiste nell'elevamento al quadrato di tutti i valori delle coordinate. Il risultato di questa operazione, riportato nel secondo grafico della medesima figura di cui sopra, mostra come questa trasformazione renda le due classi linearmente separabili. A questo punto è molto più facile trovare un classificatore lineare che divida gli esempi in maniera efficace. Si tenga inoltre presente che questa modifica delle feature non altera il problema, che rimane esattamente lo stesso.
