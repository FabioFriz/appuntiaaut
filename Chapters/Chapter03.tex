%************************************************
\chapter{Oltre la classificazione binaria}\label{ch:over_bin_class}
%************************************************

Il passo successivo al problema della classificazione binaria affrontato nel precedente Capitolo \ref{ch:bin_class} è il passaggio alla classificazione multiclasse, ovvero con più di due classi.

\section{Classificazione multiclasse}

Vi sono principalmente due metodi per costruire modelli che risolvano task di questo tipo: nel primo caso si cerca di costruire un algoritmo di apprendimento in grado di classificare su più classi, nel secondo si combinano più classificatori binari (algoritmi di classificazione binari). Questo secondo approccio sarà quello affrontato nel presente Capitolo.

Anche le misure delle performance di un classificatore binario ampiamente presentate nella sottosezione \ref{ssec:bin_class_performances} verranno utilizzate per valutare algoritmi che lavorano con più classi, si possono infatti costruire matrici di contingenza per più di due classi di cui un esempio è riportato nella seguente Tabella \ref{tab:multiclass_performances}.

\begin{table}[ht]
	\hskip-2cm
	\begin{tabular}{r|cccc|c}
		& \multicolumn{3}{c}{Predetti} &  & Recall per classe \\
		& $ C_{1} $ & $ C_{2} $ & $ C_{3} $ & & \\
		\hline
		$ C_{1} $ & $ \textbf{15} $ & $ 2 $ & $ 3 $ & $ 20 $ & $ \frac{15}{20} = 0.75 $ \\
		Attuali $ C_{2} $ & $ 7 $ & $ \textbf{15} $ & $ 8 $ & $ 30 $ & $ \frac{15}{30} = 0.5 $ \\
		$ C_{3} $ & $ 2 $ & $ 3 $ & $ \textbf{45} $ & $ 50 $ & $ \frac{45}{50} = 0.9 $ \\
		& $ 24 $ & $ 20 $ & $ 56 $ & $ 100 $ &  \\
		\hline
		Precisione per classe & $ \frac{\textbf{15}}{24} = 0.63 $ & $ \frac{\textbf{15}}{20} = 0.75 $ & $ \frac{\textbf{45}}{56} = 0.80 $ &  & $ \textit{acc} = \frac{15+15+45}{100} = 0.75 $ \\
	\end{tabular}
	\caption{Tabella di contingenza costruita per un esempio con $ 3 $ classi con alcune misure delle performance (recall e precisione) per ciascuna classe e la accuratezza finale.}
	\label{tab:multiclass_performances}
\end{table}

Sebbene questa tabella di contingenza sia più grande la sua lettura rimane molto facile: nelle colonne ci sono le previsioni per ogni classe fatte dal modello, sule righe invece l'effettiva suddivisione degli esempi per classi, sulla diagonale sono evidenziati i valori corrispondenti al numero di istanze classificate correttamente per ogni classe, la quarta riga riporta le somme dei valori di ogni colonna e la quarta colonna le somme dei valori di ogni riga. La Tabella \ref{tab:multiclass_performances} è stata ulteriormente arricchita con alcune misure più importanti delle performance: l'accuratezza del modello, la precisione del modello per ogni classe e la recall del modello per ogni classe.

Ci sono numerosi modi per combinare più classificatori binari per ottenere un classificatore multiclasse, altrimenti noto come \textit{k-class classifier}, che possono essere riassunti principalmente sotto due famiglie:
\begin{itemize}
	\item schemi \textit{one-vs-rest}:
	\begin{itemize}
		\item non ordinati;
		\item ordinati;
	\end{itemize}
	\item schemi \textit{one-vs-one}:
	\begin{itemize}
		\item simmetrici;
		\item asimmetrici.
	\end{itemize}
\end{itemize}

Di seguito viene presentato un esempio di apprendimento con un dataset di istanze etichettate con $ 3 $ classi applicando e confrontando fra di loro questi quattro diversi approcci.

\subsection{One-vs-rest - Non ordinato}\label{ssec:ovr_unord}

Il primo approccio per la risoluzione del task di classificazione multiclasse prevede la combinazione di tre diversi classificatori binari come riportato in Figura \ref{fig:onevsrest_unord}. Questo si traduce in tre diversi problemi di apprendimento, uno per ciascun classificatore binario $ \hat{\textit{c}}_{1} $, $ \hat{\textit{c}}_{2} $ e $ \hat{\textit{c}}_{3} $ isolando in ciascuno una delle $ 3 $ diverse classi.

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.8\textwidth]{Images/OneVsRest_Unord}
	\caption{Esempio di utilizzo di tre classificatori binari per risolvere un problema di classificazione con $ 3 $ classi. A sinistra è riportato il dataset iniziale, a destra i tre classificatori utilizzati per la classificazione con schema one-vs-rest non ordinata.}
	\label{fig:onevsrest_unord}
\end{figure}

In ciascuno dei tre problemi di apprendimento i dati iniziali rimangono invariati, fatta eccezione per le etichette che vengono aggiornate in base alla classe che viene isolata. A questo punto le etichette avranno un valore binario: $ +1 $ se l'istanza appartiene alla classe isolata, $ -1 $ se invece appartiene ad una delle altre due.

Tale schema può essere riassunto più formalmente con una \textit{output-code matrix}, ogni riga corrisponde ad una classe e ogni colonna ad un classificatore binario. In altre parole attraverso tale matrice si specifica per ogni classificatore quale classe esso sarà in grado di distinguere, intuitivamente utilizzando l'etichetta $ +1 $, e quali invece no con $ -1 $, implicando la sostituzione delle etichette originarie per il dataset di ogni classificatore.

\begin{figure}
	\centering
	\includegraphics[width=0.4\textwidth]{Images/OneVsRest_Unord_Mat}
\end{figure}

Tale matrice è utile anche per analizzare i risultati del classificatore multiclasse. Quest'ultimo restituisce un vettore composto dalle etichette binarie, una per ciascun classificatore binario che lo compone, che identifica la classe predetta. Confrontando tale vettore con le righe della matrice si risale alla classe predetta.

\subsection{One-vs-rest - Ordinato}

Per ridurre la mole di lavoro si cerca di sfruttare un ordinamento: si svolge il primo confronto con il primo classificatore, se questo riconosce l'esempio come appartenente alla classe $ \textit{C}_{1} $ il problema è risolto, altrimenti si rimane in dubbio fra le altre due classi. In questo caso si può sfuttare un secondo classificatore che si concentra solo su queste due.

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.3\textwidth]{Images/OneVsRest_Ord_Mat}
\end{figure}

\subsection{One-vs-one - Simmetrico}

Nei primi due approcci i modelli isolavano una classe contro tutte le altre, al contrario in questo e nel prossimo schema si confronteranno le classi a coppie, una contro una. Anche in questo caso la costruzione della matrice è molto semplice: ogni riga corrisponde ad una classe, ogni colonna ad un classificatore binario. I valori nulli corrispondono a classi non contemplate dal modello.

Questo approccio è simmetrico perché un modello che distingua fra la classe $ \textit{C}_{1} $ e la classe $ \textit{C}_{2} $ non considera l'ordine delle classi, il cui cambiamento richiederebbe solo l'inversione dei segni dei risultati.

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.4\textwidth]{Images/OneVsOne_Sym_Mat}
\end{figure}

\subsection{One-vs-one - Asimmetrico}

Tuttavia possono presentarsi dei casi in cui l'ordine con cui vengono consideratele le classi è rilevante, pertanto con l'approccio asimmetrico si considerano tutti i classificatori e i rispettivi speculari per un risultato più robusto.

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.6\textwidth]{Images/OneVsOne_Asym_Mat}
\end{figure}

\subsection{Classificazione con le matrici output-code}

Per poter svolgere la classificazione è quindi necessario disporre della matrice output-code. Come anticipato infatti il vettore risultante per un'istanza va confrontato con ogni riga della matrice. Vince la classe associata alla riga più vicina al vettore risultante. Il concetto di "vincita" può essere declinato in vari modi, di seguito viene riportato quello maggiormente in uso.

Data la classe corretta $ \textit{c}_{\textit{i}} $ e il vettore risultante dal classificatore $ \textit{w}_{\textit{i}} $ per una istanza, allora è possibile calcolare la distanza come:
\begin{equation}
\textit{d}(\textit{w}, \textit{c}) = \displaystyle\sum_{\textit{i}} \dfrac{(1 - \textit{c}_{\textit{i}}\textit{w}_{\textit{i}})}{2} \nonumber
\end{equation}

Se ad esempio si considera per una certa istanza il vettore risultante $ \textit{w} = (+1, -1, -1) $, allora le distanze fra questo e le tre righe della matrice vista nella sottosezione \ref{ssec:ovr_unord} saranno:
\begin{eqnarray}
\textit{d}(\textit{w}, \textit{C}_{1}) = \dfrac{(1 - (+1) \cdot (+1)) + (1 - (-1) \cdot (-1)) + (1 - (-1) \cdot (-1))}{2} = 0 \nonumber \\
\textit{d}(\textit{w}, \textit{C}_{2}) = \dfrac{(1 - (-1) \cdot (+1)) + (1 - (+1) \cdot (-1)) + (1 - (-1) \cdot (-1))}{2} = 2 \nonumber \\
\textit{d}(\textit{w}, \textit{C}_{3}) = \dfrac{(1 - (-1) \cdot (+1)) + (1 - (-1) \cdot (-1)) + (1 - (+1) \cdot (-1))}{2} = 2 \nonumber
\end{eqnarray}

\subsection{Difficoltà applicative dei diversi approcci}

Tenendo presente che la scelta dell'approccio è sempre basata sul dominio, sui dati a disposizione e su eventuali test, nel guidare tale decisione si possono tenere conto delle difficoltà che hanno le due famiglie di approcci che sono state prese in considerazione.

Per gli approcci basati sullo schema \textit{one-vs-rest} ci sono problemi di bilanciamento nei dataset utilizzati nei singoli classificatori binari, problemi che persistono anche se il dataset di partenza è ben bilanciato. \`{E} molto facile fare un esempio che renda palese lo sbilanciamento di cui si parla. Si prenda in considerazione un dataset con $ 10 $ classi e $ 10 $ istanze per ciascuna classe, per un totale di $ 100 $ istanze: è un dataset perfettamente bilanciato, l'ideale. Tuttavia applicando l'approccio \textit{one-vs-rest} il dataset viene diviso in modo molto sbilanciato perché una classe isolata avrà solamente $ 10 $ istanze, contro le $ 90 $ delle altre $ 9 $ classi contro cui si pone. Tale problema non va sottovalutato perché crea difficoltà alla maggior parte degli algoritmi di apprendimento.

Il problema appena rilevato ovviamente non si pone nel caso di approcci basati sullo schema \textit{one-vs-one} grazie all'introduzione dell'etichetta $ 0 $ per le istanze che non appartengono a nessuna delle due classi prese in considerazione dal classificatore. Tuttavia anche in questo caso si ha un problema da non sottovalutare: la riduzione del dataset. Considerando infatti le classi a coppie si riduce il numero di istanze da utilizzare per l'addestramento, ché in caso di dataset poco popolati in partenza crea gravi difficoltà agli algoritmi di apprendimento con conseguenti performance ridotte.

\section{Regressione}

I task di regressione possono essere visti in un'ottica di evoluzione della classificazione vista fino ad ora. In questo problema di apprendimento non si ha più un limitato numero di classi, l'output richiesto ora ha un dominio molto più ampio, quello dei valori continui: $ \mathds{R} $. Si ha quindi una funzione di stima, anche nota come regressore (in inglese \textit{regressor}), definita come:
\begin{equation}
\hat{\textit{f}} : \mathscr{X} \rightarrow \mathds{R} \nonumber
\end{equation}

Il problema di apprendimento della regressione quindi è imparare una funzione di stima dagli esempi $ \textit{x}_{\textit{i}} $ ai quali sono associati valori continui dati dalla vera funzione $ \textit{f} $\footnote{Si ricorda che il cappelletto sulla funzione $ \hat{\textit{f}} $ indica che si tratta di una approssimazione della vera funzione $ \textit{f} $.}: $ (\textit{x}_{\textit{i}}, \textit{f}(\textit{x}_{\textit{i}})) $.

Cambiando l'obbiettivo della funzione da un numero relativamente ristretto di classi ad un infinito spazio delle soluzioni, l'algoritmo cercherà di stimare con la massima precisione i valori associati ad ogni esempio, portandosi sicuramente verso il problema dell'\textit{overfitting}. Tenendo presente questo ci si deve preparare psicologicamente ad accettare un bilanciamento fra precisione ed approssimazione delle soluzioni, è inevitabile a fronte delle normali fluttuazioni dei dati nel dataset che il modello non può catturare con esattezza.

Il vero scopo del regressore, se non può essere troppo preciso senza peccare di overfitting, è riuscire ad approssimare nel migliore dei modi l'andamento della funzione $ \textit{f} $.

\begin{figure}
	\centering
	\includegraphics[width=1\textwidth]{Images/RegressionApprox}
	\caption{Diverse funzioni che cercano di approssimare l'andamento dei cinque punti riportati nel grafico. Modelli più complessi sono più precisi, con il rischio dell'overfitting, modelli più semplici sono meno precisi, ma possono generalizzare meglio.}
	\label{fig:regression_approx}
\end{figure}

\subsection{Il problema dell'overfitting II - Il ritorno}

Era stato già introdotto e discusso nella sottosezione \ref{ssec:overfitting}, ora il problema dell'overfitting fa di nuovo ritorno con l'argomento della regressione e delle funzioni reali.

Un esempio di quanto appena descritto si può vedere nella Figura \ref{fig:regression_approx} dove si ha un grafico con cinque punti e si vuole trovare la funzione che riesce a passare per tutti questi. Fra le cinque funzioni disegnate ve ne sono alcune molto precise, che toccano esattamente tutti i punti o vi passano molto vicino, ma peccano di overfitting e non generalizzano adeguatameente oltre ad essere anche complesse. Al contrario ci sono funzioni molto più semplici (la più semplice è una retta) che non soffrono di overfitting, generalizzano maggiormente, e anche se non toccano tutti i punti vi passano comunque abbastanza vicino da poter commettere un errore contenuto.

Se si considera un generico dataset con $ \textit{n} $ istanze, una funzione polinomiale che passi per tutte queste dovrebbe essere di grado $ \textit{n} - 1 $. Come ormai si sa bene, una soluzione del genere sarebbe perfetta per il training set, ma non generalizzerebbe adeguatamente per avere performance soddisfacenti anche sul test set. E questo è male. Senza contare che i dataset non contengono poche istanze, quindi stimare funzioni di grado molto alto può anche rivelarsi complesso e costoso.

Per evitare quindi il problema dell'overfitting e abbracciare la generalizzazione tanto agognata bisogna ridurre il numero dei parametri stimati, ovvero il grado della funzione di stima, per ottenere un modello più semplice secondo il principio del \textit{Novacula Occami}\footnote{Altrimenti noto come rasoio di Occam.}.

\subsection{Il problema \textit{bias-variance}}\label{ssec:biasvariance}

Sulla base di quanto appena discusso circa l'overfitting e la semplificazione dei modelli, si potrebbe essere indotti a credere che questo principio debba guidare sempre la risoluzione dei problemi di apprendimento verso modelli tendenzialmente lineari, i più semplici che ci sono. Questa è una conclusione inesatta, ed è il nocciolo del \textit{bias-variance dilemma}.

Non basta infatti puntare al modello più semplice, perché questo potrebbe rivelarsi insufficiente per minimizzare le penalità ed ottenere un modello in grado di svolgere con accuratezza le predizioni.

Si può quindi concludere che modelli molto complessi soffrono della varianza (\textit{variance}) dei dati nel dataset ma sono molto precisi, fino all'overfitting, e non hanno bisogno di alcun \textit{bias}; al contrario modelli molto semplici non patiscono la varianza, ma al contrario devono introdurre un \textit{bias} che non può essere nemmeno risolto da grandi quantità di dati in input.

Alla luce di quanto appena detto si può quindi analizzare l'errore che un modello commette e distinguerne due cause. I modelli troppo semplici sono maggiormente soggetti ad errori dovuti alla mancanza di complessità che impedisce loro di stimare con sufficiente precisione; i modelli troppo complessi che invece commettono errori perché si adattano troppo ai dati e quindi vengono penalizzati non appena le istanze si discostano anche poco.

Questi concetti di bias e varianza visti fino ad ora possono essere formalizzati. L'errore quadratico atteso dal regressore $ \hat{\textit{f}}(\textit{x}) $ per una singola istanza $ \textit{x} $ del dataset può essere decomposto con la seguente formula:
\begin{eqnarray}
\textit{E}\left[ \left( \textit{f}(\textit{x}) - \hat{\textit{f}}(\textit{x}) \right)^{2} \right] &=& \left(\textit{f}(\textit{x}) - \textit{E}[\hat{\textit{f}}(\textit{x})]\right)^{2} + \textit{E}\left[\left(\hat{\textit{f}}(\textit{x}) - \textit{E}[\hat{\textit{f}}(\textit{x})]\right)^{2}\right] \nonumber \\
&=& Bias^{2}\left(\hat{\textit{f}}(\textit{x})\right) + Var\left(\hat{\textit{f}}(\textit{x})\right) \label{eqn:bias_var}
\end{eqnarray}

All'interno di questa formula è possibile identificare due principali elementi:
\begin{itemize}
	\item $ \left(\textit{f}(\textit{x}) - \textit{E}[\hat{\textit{f}}(\textit{x})]\right)^{2} $ questo primo termine è il bias, vale $ 0 $ se il regressore è mediamente corretto, ed è il quadrato della differenza (ovvero la distanza) fra la funzione $ \textit{f} $ corretta e il valore medio del modello $ \hat{\textit{f}} $ che si sta costruendo;
	\item $ \textit{E}\left[\left(\hat{\textit{f}}(\textit{x}) - \textit{E}[\hat{\textit{f}}(\textit{x})]\right)^{2}\right] $ questo secondo termine è la varianza, ovvero il quadrato della differenza attesa tra la funzione stimata e la funzione stimata attesa, è l'errore causato dalle variazioni dei dati rispetto alla loro media.
\end{itemize}

La dimostrazione che è vera la Formula \ref{eqn:bias_var} a partire dal termine a sinistra dell'uguale viene di seguito riportata.
\begin{eqnarray}
\textit{E}\left[ \left( \textit{f}(\textit{x}) - \hat{\textit{f}}(\textit{x}) \right)^{2} \right] &=& \textit{E}\left[\textit{f}(\textit{x})^{2} - 2\textit{f}(\textit{x})\hat{\textit{f}}(\textit{x}) + \hat{\textit{f}}(\textit{x})^{2}\right] \nonumber \\
&=& \textit{E}\left[\textit{f}(\textit{x})^{2}\right] - 2\textit{f}(\textit{x})\textit{E}\left[\hat{\textit{f}}(\textit{x})\right] + \textit{E}\left[\hat{\textit{f}}(\textit{x})^{2}\right] \nonumber \\
&=& \textit{E}\left[\textit{f}(\textit{x})^{2}\right] - 2\textit{f}(\textit{x})\textit{E}\left[\hat{\textit{f}}(\textit{x})\right] + \textit{E}\left[\hat{\textit{f}}(\textit{x})^{2}\right] - \textit{E}\left[\hat{\textit{f}}(\textit{x})\right]^{2} + \textit{E}\left[\hat{\textit{f}}(\textit{x})\right]^{2} \nonumber \\
&=& \textit{E}\left[\hat{\textit{f}}(\textit{x})^{2}\right] - \textit{E}\left[\hat{\textit{f}}(\textit{x})\right]^{2} + \textit{f}(\textit{x})^{2} - 2\textit{f}(\textit{x})\textit{E}\left[\hat{\textit{f}}(\textit{x})\right] + \textit{E}\left[\hat{\textit{f}}(\textit{x})\right]^{2} \nonumber \\
&=& \textit{E}\left[\left(\hat{\textit{f}}(\textit{x}) - \textit{E}\left[\hat{\textit{f}}(\textit{x})\right]\right)^{2}\right] + \left(\textit{f}(\textit{x}) - \textit{E}\left[\hat{\textit{f}}(\textit{x})\right]\right)^{2} \nonumber \\
&=& Var\left(\hat{\textit{f}}(\textit{x})\right) + Bias^{2}\left(\hat{\textit{f}}(\textit{x})\right) \nonumber
\end{eqnarray}

Le operazioni che vengono svolte sono molto semplici:
\begin{itemize}
	\item nel primo passaggio viene svolto il quadrato del binomio;
	\item nel secondo passaggio l'operatore $ \textit{E} $ viene distribuito sui singoli addendi ottenuti poc'anzi;
	\item nel terzo passaggio viene tirato fuori dal secondo addendo la componente costante che non varia nella stima, ovvero $ 2\textit{f}(\textit{x}) $, e poi viene semplicemente sommata e sottratta una stessa quantità che si rivelerà utile in seguito;
	\item nel quarto passaggio viene tirato fuori dall'operatore $ \textit{E} $ anche $ \textit{f}(\textit{x})^{2} $ poiché anche in questo caso trattandosi della funzione vera è una quantità costante, vengono anche riordinati i diversi termini;
	\item nel quinto passaggio i primi due termini sono una forma equivalente della varianza\footnote{
		Dalla Formula della varianza $\textit{E}\left[\left(\textit{x} - \textit{E}\left[\textit{x}\right]\right)^{2}\right]$ è possibile sviluppare il quadrato e ottenere $\textit{E}\left[x^2-2xE\left[x\right]+\left[E(x)\right]^2\right]$ che, sfruttando la linearità del valore atteso e redistribuendolo, regalerà la formula $\left[E\left[x^2\right]-2E\left[x\right]E\left[x\right]+\left[E(x)\right]^2\right]$ a cui possono essere applicati semplici passaggi algebrici per ottenere la formula equivalente della varianza:
		\begin{eqnarray}
		\left[E\left[x^2\right]-2E\left[x\right]E\left[x\right]+\left[E(x)\right]^2\right] &=& \left[E\left[x^2\right]-2\left(E\left[x\right]\right)^2+\left[E(x)\right]^2\right] \nonumber \\
		&=& E\left[x^2\right]-\left(E\left[x\right]\right)^2 \nonumber
		\end{eqnarray}
	}, gli altri tre invece vengono riscritti come quadrato di una differenza;
	\item nel sesto ed ultimo passaggio vengono risconosciute sia la varianza che il quadrato del bias. QED.
\end{itemize}

\section{Apprendimento non supervisionato e modelli descrittivi}

Fino a questo momento sono stati tenuti in considerazione quasi esclusivamente i task di tipo predittivo, ovvero quelli il cui scopo era costruire un modello in grado di svolgere previsioni corrette su determinate classi di istanze.

In questa sezione si vogliono affrontare quindi i task di tipo descrittivo, che nella sottosezione \ref{ssec:descr_tasks} erano stati brevemente introdotti, e in aggiunta a questi in generale i task correlati all'apprendimento non supervisionato. Questi due argomenti infatti sono parzialmente sovrapposti, in quanto come si poteva già vedere dalla Tabella \ref{tab:tasks} l'apprendimento non supervisionato può essere utilizzato proprio per ottenere modelli di tipo descrittivo.

Se nei task di tipo predittivo si avevano a disposizione dati etichettati, training set e test set, in quelli di tipo descrittivo la situazione è diversa, in particolare le differenze sono principalmente due:
\begin{itemize}
	\item nell'apprendimento descrittivo non si dispone di dati etichettati, ma di dati "e basta", pertanto non si distingue più un training set;
	\item lo scopo dell'apprendimento descrittivo è apprendere un modello in grado di descrivere i dati e non predire la classe di dati mai visti.
\end{itemize}

\begin{figure}[ht]
\centering
\includegraphics[width=1.05\textwidth]{Images/MachineLearning_Overview2}
\caption{Schema riassuntivo del funzionamento del machine learning per task di tipo descrittivo, che si differenzia dallo schema già visto in Figura \ref{fig:aaut_schema} per i task di tipo predittivo.}
\label{fig:descr_task_schema}
\end{figure}

Si può quindi affermare che per i problemi di apprendimento descrittivo il task corrisponde esattamente al learning problem, proprio come si può vedere nella Figura \ref{fig:descr_task_schema}. In tale immagine si possono notale le differenze appena presentate anche tramite il confronto con la Figura \ref{fig:aaut_schema} che invece era riferita ai task di tipo predittivo.

L'assenza di etichette associate ai dati cui si è fatto cenno poc'anzi è una costante nell'apprendimento non supervisionato.

\subsection{Clustering predittivo vs descrittivo}

Lo scopo del clustering è di trarre conoscenza dai dati scoprendo al loro interno gruppi omogenei nei quali vengono condivisi alcuni aspetti comuni.

Il clustering tuttavia può essere di due tipi principalmente, entrambi rientranti sotto il cappello dell'apprendimento non supervisionato:
\begin{description}
	\item[clustering predittivo] Processo di apprendimento di una funzione in grado di etichettare correttamente istanze dopo un addestramento su dati privi di etichette. Dato un insieme di nuove etichette $ \mathscr{C} = \{\textit{C}_{1}, \textit{C}_{2}, ..., \textit{C}_{\textit{k}}\} $ e lo spazio delle istanze $ \mathscr{X} $, il cluster si può essere formalizzato come:
	\begin{equation}
	\hat{\textit{q}} : \mathscr{X} \rightarrow \mathscr{C} \nonumber
	\end{equation}
	\item[clustering descrittivo] Processo di apprendimento di un insieme di etichette $ \mathscr{C} = \{\textit{C}_{1}, \textit{C}_{2}, ..., \textit{C}_{\textit{k}}\} $ a partire da un certo dataset $ \textit{D} $:
	\begin{equation}
	\hat{\textit{q}} : \textit{D} \rightarrow \mathscr{C} \nonumber
	\end{equation}
\end{description}

\`{E} importante rimarcare come il clustering, il buon clustering, deve raggruppare i dati in gruppi coerenti, all'interno dei quali si trovino istanze più simili fra loro piuttosto che a quelle appartenenti a gruppi diversi. Proprio in quest'ottica il concetto chiave che permette di identificare cluster adeguati in base al dominio e alle esigenze è la \textit{distanza}, in particolare \textit{come} questa viene calcolata.

La distanza fra due istanze viene misurata sulle loro feature, e può essere una distanza euclidea, oppure una distanza di Mahalanobis che verrà affrontata in seguito. Tale misura non è limitata a feature numeriche, può essere infatti estesa ad altri tipi di dati.

Il clustering e in generale i modelli basati sulla distanza verranno affrontati in dettaglio nel Capitolo \ref{ch:distbased_models}.

\subsection{Subgroup discovery}

Premesso che non è chiarissimo come sia finita all'interno di una sezione dedicata all'apprendimento non supervisionato, in questa sottosezione si introducono i task di \textit{subgroup discovery} che rientrano nella famiglia degli algoritmi di apprendimento supervisionato.

Dato un dataset etichettato $ \{(\textit{x}, \textit{l}(\textit{x}))\} $, lo scopo di tali task è trovare una funzione che si definisce come:
\begin{equation}
\hat{\textit{g}} : \textit{D} \rightarrow \{\textit{true}, \textit{false}\} \nonumber
\end{equation}
tale che:
\begin{equation}
\textit{G} = \{\textit{x} \in \textit{D}\ |\ \hat{\textit{g}}(\textit{x}) = \textit{true}\} \nonumber
\end{equation}
ha una distribuzione nelle classi fortemente differente dalla popolazione del dataset originario $ \textit{D} $. L'insieme di istanze $ \textit{G} $ è quindi un sottoinsieme di $ \textit{D} $ è si chiama \textit{estensione}.

Si osserva inoltre che avendo la funzione $ \hat{\textit{g}} $ un codominio booleano è possibile ricondurre la ricerca di sottogruppi a problemi di classificazione binaria, permettendo quindi l'utilizzo dei classificatori binari di cui si è già ampiamente scritto.

Gli algoritmi di subgroup discovery vengono guidati da una misura di valutazione. Di queste ne esistono molte, ma la maggior parte condivide due caratteristiche:
\begin{itemize}
	\item vengono preferiti grandi sottogruppi;
	\item sono solitamente simmetriche, quindi viene riportato lo stesso valore per un sottogruppo e per il suo complementare.
\end{itemize}

\subsection{Association rule discovery}

Siano $ \textit{b} $ e $ \textit{h} $ due insiemi di attributi o due valori. Dato un dataset $ \textit{D} $ senza alcuna etichetta, lo scopo dei task di association rule discovery è trovare un insieme di \textit{regole} della forma $ \{\textit{b} \rightarrow \textit{h}\} $ tali che $ \textit{b} \cup \textit{h} $ sia frequente e che $ \textit{h} $ sia spesso presente dove compare $ \textit{b} $.

Per scoprire regole di associazione si rendere in primo luogo necessario identificare insiemi di istanze frequenti, a questo punto è possibile valutare tutte le regole frequenti per tali insiemi. Se ad esempio l'insieme di istanze $ \{\textit{i}_{1}, \textit{i}_{2}, \textit{i}_{3}\} $ è molto frequente, si possono identificare tutte le seguenti regole:
\begin{itemize}
	\item[] $ \textit{i}_{1} \rightarrow \textit{i}_{2}, \textit{i}_{3} $
	\item[] $ \textit{i}_{2} \rightarrow \textit{i}_{1}, \textit{i}_{3} $
	\item[] $ \textit{i}_{3} \rightarrow \textit{i}_{1}, \textit{i}_{2} $
	\item[] $ \textit{i}_{1}, \textit{i}_{2} \rightarrow \textit{i}_{3} $
	\item[] $ \textit{i}_{1}, \textit{i}_{3} \rightarrow \textit{i}_{2} $
	\item[] $ \textit{i}_{2}, \textit{i}_{3} \rightarrow \textit{i}_{1} $
\end{itemize}

Tuttavia fra tante regole bisogna selezionare quelle migliori. Quali siano le migliori può essere deciso con una misura della confidenza che fa uso di una \textit{support threshold}\footnote{Tale \textit{support threshold} può essere interpretata come una soglia sotto la quale non si considera frequente un elemento.}:
\begin{equation}
\dfrac{supp(\textit{b} \cup \textit{h})}{supp(\textit{b})} \nonumber
\end{equation}

Tale approccio basato sulla forza bruta porta rapidamente ad una quantità di regole enorme, un impiego di tempo esponenziale se si tiene conto che si deve calcolare il valore di \textit{support} per ciascun elemento, identificare tutti gli insiemi di elementi frequenti e per ciascuno di questi tutte le regole possibili.

Tuttavia esistono algoritmi molto efficienti per i task di scoperta delle regole di associazione che si basano sul fatto che se un insieme di elementi non è frequente, allora non lo è nemmeno nessuno dei suoi soprainsiemi.
